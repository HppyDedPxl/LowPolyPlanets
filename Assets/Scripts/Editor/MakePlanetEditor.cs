﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

[CustomEditor(typeof(MakePlanet))]
public class MakePlanetEditor : Editor {

    int m_refinement;

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        MakePlanet mp = (MakePlanet)target;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Refinement Level: ");
        m_refinement = EditorGUILayout.IntField(m_refinement);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Generate Planet")) {
            mp.GeneratePlanet(m_refinement);
        }
    }

    void OnSceneGUI() {
      //  var _target = (MakePlanet)target;
      
        //for (int i = 0; i < _target.icoData.faceData.Length; i++) {
        //    Handles.Label(_target.icoData.faceData[i].center, "Nr: " + i);
        //}

    }
}


