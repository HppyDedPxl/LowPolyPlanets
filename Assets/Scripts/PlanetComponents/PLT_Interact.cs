﻿using UnityEngine;
using System.Collections;

public class PLT_Interact : MonoBehaviour {

    PLT_Planet parentPlanet;

    public void Initialize(PLT_Planet parent) {
        parentPlanet = parent;
    }


    int curClick = 0;
    PLT_Field from;
    PLT_Field to;
    PLT_Field[] Path;
    void Update() {

        if (Input.GetMouseButtonDown(0)) {
            Ray screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit info;
            bool wasChanged = false;
            if (Physics.Raycast(screenRay, out info, Mathf.Infinity)) {
                PLT_Field f = parentPlanet.GetHitField(info.point);


                //  wasChanged = GetComponent<PLT_Terraformer>().MakeFieldsGrass(new PLT_Field[] { f,f.AdjacentAB,f.AdjacentAC,f.AdjacentBC });

                if (curClick == 0) {
                    curClick++;
                    from = f;
                } else if (curClick == 1) {
                    curClick++;
                    to = f;
                    PLT_PathfindingResult path = GetComponent<PLT_Pathfinding>().FindPath(from, to,PLT_Pathfinding.PathSmoothingFunctions.AverageSmooth);

                   // Debug.Log("Path Length From " + from.center + " To " + to.center + " : " + path.Length);
                    for (int i = 0; i < path.WalkPathData.Length; i++) {
                        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        go.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                        go.transform.position = parentPlanet.LocalToWorldPoint(path.WalkPathData[i]);
                        go.transform.parent = parentPlanet.transform;
                    }
                }


                //  if(wasChanged) parentPla/net.UpdateMesh();
            }

        }


        // Debug

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        if (h!=0) {
            parentPlanet.transform.RotateAround(Vector3.up, h * Time.deltaTime * 3.141f * 0.5f);
        }
        if (v != 0) {
            parentPlanet.transform.RotateAround(Vector3.right, v * Time.deltaTime * 3.141f * 0.5f);

        }

    }

    void OnDrawGizmos(){
        //if (Path == null) return;
        //Gizmos.color = Color.red;
        //for (int i = 1; i < Path.Length; i++) {
        //    Gizmos.DrawLine(parentPlanet.LocalToWorldPoint(Path[i - 1].center), parentPlanet.LocalToWorldPoint(Path[i].center));
        //}
    }
}
