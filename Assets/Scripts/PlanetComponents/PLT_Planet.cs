﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class PLT_Planet : MonoBehaviour {

    public PLT_NoiseGen RNG;

    MeshRenderer m_meshRenderer;
    MeshFilter m_meshFilter;
    MeshCollider m_meshCollider;

    PLT_Field[] m_fields;

    public Vector3[] VertexDatabase;
    public Color[] PlanetColorDatabase;

    PLT_Interact m_interactModule;
    PLT_Terraformer m_terraformModule;
    PLT_Factions m_factionModule;
    PLT_Pathfinding m_pathFindingModule;
    
    public void Initialize(List<PLT_Field> fields, Vector3[] VertexDatabase,string seed) {

        m_meshRenderer = GetComponent<MeshRenderer>();
        m_meshFilter = GetComponent<MeshFilter>();
        m_meshCollider = GetComponent<MeshCollider>();
        m_fields = fields.ToArray();

        RNG = new PLT_NoiseGen();
        RNG.Octaves = 4;
        RNG.Persistance = 1 / 64;
        RNG.Seed = seed;

        this.VertexDatabase = VertexDatabase;
        PlanetColorDatabase = new Color[VertexDatabase.Length];
        for (int i = 0; i < VertexDatabase.Length; i++) {
            PlanetColorDatabase[i] = Color.white;
        }


        m_interactModule = gameObject.AddComponent<PLT_Interact>();
        m_interactModule.Initialize(this);

        m_terraformModule = gameObject.AddComponent<PLT_Terraformer>();
        m_terraformModule.Initialize(this);

        m_factionModule = gameObject.AddComponent<PLT_Factions>();
        m_factionModule.Initialize(this);

        m_pathFindingModule = gameObject.AddComponent<PLT_Pathfinding>();
        m_pathFindingModule.Initialize(this);

    }

    public PLT_Field[] GetFieldData() {
        return m_fields;
    }

    public PLT_Field GetHitField(Vector3 WorldHitPoint) {
        WorldHitPoint = transform.worldToLocalMatrix * WorldHitPoint;

        int closest = 0;
        float closestSqrDist = 9999;
        for (int i = 0; i < m_fields.Length; i++) {
            float dist = Vector3.SqrMagnitude(m_fields[i].center - WorldHitPoint);
            if (dist < closestSqrDist) {
                closestSqrDist = dist;
                closest = i;
            }
        }

        return m_fields[closest];
    }

    public Vector3 WorldHitToLocalPoint(Vector3 v) {
        return transform.worldToLocalMatrix * v;
    }

    public Vector3 LocalToWorldPoint(Vector3 v) {
        return transform.localToWorldMatrix * v;
    }

    public Mesh GetMeshData() {
        return m_meshFilter.mesh;
    }

    public void WriteMeshData(Mesh m) {
        m_meshFilter.mesh = m;
    }

    public void RebuildCollisionMesh() {
        m_meshCollider.sharedMesh = m_meshFilter.mesh;
    }

    public void UpdateCollisionMesh() {
        m_meshCollider.sharedMesh = m_meshFilter.mesh;
    }

    public void UpdateMesh() {
        Mesh m = new Mesh();

        List<Vector3> verts = new List<Vector3>();
        List<int> indices = new List<int>();
        List<Color> cols = new List<Color>();
        List<int> customNormalIndices = new List<int>();
        Vector3[] CustomNormals ;

        PLT_Tuple<List<Vector3>, List<Color>> temp = new PLT_Tuple<List<Vector3>, List<Color>>();

        int curIndex = 0;
        for (int i = 0; i < m_fields.Length ; i++) {

            temp = m_fields[i].GetFaces(ref VertexDatabase, ref PlanetColorDatabase);
            int newVerts = temp.key.Count;

            // mark main face for normal smoothing
            for (int j = 0; j < 3; j++) {
                indices.Add(curIndex);
                customNormalIndices.Add(curIndex);
                curIndex++;
            }
            // add rest
            for (int j = 0; j < temp.key.Count-3; j++) {
                indices.Add(curIndex);
                curIndex++;
            }
            verts.AddRange(temp.key);
            cols.AddRange(temp.val);
        }



        m.vertices = verts.ToArray();
        m.triangles = indices.ToArray();

        m.colors = cols.ToArray();
        m.RecalculateNormals();
        CustomNormals = m.normals;

        for (int i = 0; i < customNormalIndices.Count; i++) {

            CustomNormals[customNormalIndices[i]] = verts[customNormalIndices[i]].normalized;

        }

        m.normals = CustomNormals;

        m_meshFilter.mesh = m;
     
    }

    public void UpdatePathfindingGraphs() {
        m_pathFindingModule.UpdatePathFindingData();
    }

    public List<PLT_Field> ExpandFieldsFrom(int iterations, PLT_Field start, float acWeight, float abWeight, float bcWeigth, List<PLT_Field> closed, ExpandCondition condition) {

        if (iterations > 0) {
            --iterations;
            closed.Add(start);
            List<PLT_Field> fields = new List<PLT_Field>();

            // AC
            if (condition(start, start.AdjacentAC) && RNG.RandomRange(0f, 1f) < acWeight) {
                if (!closed.Contains(start.AdjacentAC)) {
                    fields.Add(start.AdjacentAC);
                    fields.AddRange(
                        ExpandFieldsFrom(iterations, start.AdjacentAC, acWeight, abWeight, bcWeigth, closed, condition)
                        );
                }
            }

            // AB
            if (condition(start, start.AdjacentAB) && RNG.RandomRange(0f, 1f) < abWeight) {
                if (!closed.Contains(start.AdjacentAB)) {
                    fields.Add(start.AdjacentAB);
                    fields.AddRange(
                        ExpandFieldsFrom(iterations, start.AdjacentAB, acWeight, abWeight, bcWeigth, closed, condition)
                        );
                }
            }


            // BC

            if (condition(start, start.AdjacentBC) && RNG.RandomRange(0f, 1f) < bcWeigth) {
                if (!closed.Contains(start.AdjacentBC)) {
                    fields.Add(start.AdjacentBC);
                    fields.AddRange(
                        ExpandFieldsFrom(iterations, start.AdjacentBC, acWeight, abWeight, bcWeigth, closed, condition)
                        );
                }
            }

            return fields;
        }
        return new List<PLT_Field>();
    }

}





