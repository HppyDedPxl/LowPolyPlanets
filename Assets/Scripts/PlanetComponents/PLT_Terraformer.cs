﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PLT_Terraformer : MonoBehaviour {

    PLT_PlanetPropDB propDB;
    PLT_Planet parentPlanet;

    protected int minPoleLand= 13;
    protected int maxPoleLand = 17;
    protected int minPoleSnowSpread = 19;
    protected int maxPoleSnowSpread = 30;



    public void Initialize(PLT_Planet parent) {
        parentPlanet = parent;

        if (propDB == null) {
            propDB = GameObject.FindObjectOfType<PLT_PlanetPropDB>();
        }
    }

    void InitialTerraform() {

        PLT_Field[] fields = parentPlanet.GetFieldData();
        MakeFieldsSea(fields);
        List<PLT_Field> sea = new List<PLT_Field>();
        List<PLT_Field> sand = new List<PLT_Field>();
        List<PLT_Field> land = new List<PLT_Field>();



        PLT_NoiseGen noise = new PLT_NoiseGen();
        noise.Octaves = 4;
        noise.Persistance = 1 / 64;
        noise.Seed = "ButterGolemReloaded";

        for (int i = 0; i < fields.Length; i++) {
            float val = noise.PerlinNoise3DFast(fields[i].center.x * 1.5f, fields[i].center.y * 1.5f, fields[i].center.z * 1.5f);

            if (val < 0) {
                sea.Add(fields[i]);

            } else if (val < 0.07f) {
                sand.Add(fields[i]);

            } else {
                land.Add(fields[i]);
            }
        }

        MakeFieldsSea(sea.ToArray());
        MakeFieldsSand(sand.ToArray());
        MakeFieldsGrass(land.ToArray());


        parentPlanet.UpdateMesh();
        parentPlanet.UpdateCollisionMesh();

    }

    public void ReTerraformWithNoise(string seed, float res, float deepSeaThreshold, float seaThreshold, float sandThreshold, float grassT, float mountBaseT, float mountLowT, float mountHighT) {

        parentPlanet.RNG = new PLT_NoiseGen();
        parentPlanet.RNG.Octaves = 4;
        parentPlanet.RNG.Persistance = 1 / 64;
        parentPlanet.RNG.Seed = seed;
        PLT_Field[] fields = parentPlanet.GetFieldData();
        MakeFieldsSea(fields);

        List<PLT_Field> deepsea = new List<PLT_Field>();
        List<PLT_Field> sea = new List<PLT_Field>();
        List<PLT_Field> sand = new List<PLT_Field>();
        List<PLT_Field> land = new List<PLT_Field>();

        // Polar Regions
        PLT_Field NorthPoleCenter = parentPlanet.GetHitField(transform.up * 50);
        PLT_Field SouthPoleCenter = parentPlanet.GetHitField(transform.up * -50);


        List<PLT_Field> mountains = new List<PLT_Field>();
        List<float> mountainOffsets = new List<float>();

        parentPlanet.RNG.Seed = seed;

      
        for (int i = 0; i < fields.Length; i++) {

            if(fields[i].Prop != null)
                Destroy(fields[i].Prop);

            float val = parentPlanet.RNG.PerlinNoise3DFast(fields[i].center.x * res, fields[i].center.y * res, fields[i].center.z * res);

            if (val < deepSeaThreshold) {
                deepsea.Add(fields[i]);
            } else if (val < seaThreshold) {
                sea.Add(fields[i]);

            } else if (val < sandThreshold) {
                sand.Add(fields[i]);

            } else if (val < grassT) {
                land.Add(fields[i]);
            } else {
                mountains.Add(fields[i]);
                mountainOffsets.Add(Mathf.Max(0.04f, (val - grassT) * .5f));
            }
        }





        MakeFieldsDeepSea(deepsea.ToArray());
        MakeFieldsSea(sea.ToArray());
        MakeFieldsSand(sand.ToArray());
        MakeFieldsGrass(land.ToArray());
        // mountains
        for (int i = 0; i < mountains.Count; i++) {
            MakeFieldMountainCustomHeight(mountains[i], mountainOffsets[i]);
        }

       


        List<PLT_Field> poleFields = new List<PLT_Field>();
        List<float> poleDistances = new List<float>();
        List<PLT_Field> closed = new List<PLT_Field>();


        // Polar regions

        int poleLandSpread = parentPlanet.RNG.RandomRange(minPoleLand, maxPoleLand);
        int poleSnowSpread = parentPlanet.RNG.RandomRange(minPoleSnowSpread, maxPoleSnowSpread);

        // NORTHPOLE

        //Ensure that there is land on the pole
        if (NorthPoleCenter.FieldType == PLT_Biome.Water || NorthPoleCenter.FieldType == PLT_Biome.DeepWater) {
            poleFields.Add(NorthPoleCenter);
            poleFields.AddRange(parentPlanet.ExpandFieldsFrom(poleLandSpread, NorthPoleCenter, 1.1f, 1.1f, 1.1f, closed, PLT_Utilities.PLT_ExpandConditions.expandPolar));
            MakeFieldsGrass(poleFields.ToArray());
        }

        poleFields.Clear();
        poleDistances.Clear();
        closed.Clear();

        poleFields.Add(NorthPoleCenter);
        poleFields.AddRange(parentPlanet.ExpandFieldsFrom(poleSnowSpread, NorthPoleCenter, 0.95f, 0.95f, 0.95f, closed, PLT_Utilities.PLT_ExpandConditions.expandAlways));

        GameObject npSnow = Instantiate(propDB.SnowParticles);
        npSnow.name = "NorthPoleSnow";
        npSnow.transform.position = NorthPoleCenter.center + NorthPoleCenter.center * 0.1f;
        npSnow.transform.LookAt(NorthPoleCenter.center);
        npSnow.transform.parent = parentPlanet.transform;
        NorthPoleCenter.Prop = npSnow;


 

        // get max dist field northpole
        float maxDistPole = 0;
        for (int i = 0; i < poleFields.Count; i++) {
            float dist = Vector3.SqrMagnitude(NorthPoleCenter.center - poleFields[i].center);
            if (dist > maxDistPole) {
                maxDistPole = dist;
            }
            poleDistances.Add(dist);
        }

        for (int i = 0; i < poleFields.Count; i++) {
            MakeFieldPolarRegion(poleFields[i], poleDistances[i] / maxDistPole);
        }

        // SOUTHPOLE
        poleFields.Clear();
        poleDistances.Clear();
        closed.Clear();

        //Ensure that there is land on the pole
        if (SouthPoleCenter.FieldType == PLT_Biome.Water || SouthPoleCenter.FieldType == PLT_Biome.DeepWater) {
            poleFields.Add(SouthPoleCenter);
            poleFields.AddRange(parentPlanet.ExpandFieldsFrom(poleLandSpread, SouthPoleCenter, 1.1f, 1.1f, 1.1f, closed, PLT_Utilities.PLT_ExpandConditions.expandPolar));
            MakeFieldsGrass(poleFields.ToArray());
        }

        poleFields.Clear();
        poleDistances.Clear();
        closed.Clear();


        poleFields.Add(SouthPoleCenter);
        poleFields.AddRange(parentPlanet.ExpandFieldsFrom(poleSnowSpread, SouthPoleCenter, 0.95f, 0.95f, 0.95f, closed, PLT_Utilities.PLT_ExpandConditions.expandAlways));

        // get max dist field northpole
        maxDistPole = 0;
        for (int i = 0; i < poleFields.Count; i++) {
            float dist = Vector3.SqrMagnitude(SouthPoleCenter.center - poleFields[i].center);
            if (dist > maxDistPole) {
                maxDistPole = dist;
            }
            poleDistances.Add(dist);
        }

        for (int i = 0; i < poleFields.Count; i++) {
            MakeFieldPolarRegion(poleFields[i], poleDistances[i] / maxDistPole);
        }

        GameObject spSnow = Instantiate(propDB.SnowParticles);
        spSnow.name = "SouthPoleSnow";
        spSnow.transform.position = SouthPoleCenter.center + SouthPoleCenter.center * 0.1f;
        spSnow.transform.LookAt(SouthPoleCenter.center);
        spSnow.transform.parent = parentPlanet.transform;
        SouthPoleCenter.Prop = spSnow;

        parentPlanet.UpdateMesh();
        parentPlanet.UpdateCollisionMesh();
    }

    public void PopulatePropsForests() {
        PLT_Field[] fields = parentPlanet.GetFieldData();

        for (int j = 0; j < PLT_Config.BIOME_MOUNTAIN_PROPS_FORESTCOUNT; j++) {


            PLT_Field f = fields[0];
            while (f.FieldType != PLT_Biome.HillsAndMountains || f.Prop != null || (f.FieldType == PLT_Biome.HillsAndMountains && f.HeightOffset > PLT_Config.BIOME_MOUNTAIN_HEIGHT_HILLS))
                f = fields[parentPlanet.RNG.RandomRange(0, fields.Length)];

            List<PLT_Field> toplace = new List<PLT_Field>();
            List<PLT_Field> closed = new List<PLT_Field>();

            // Trees
            toplace.Add(f);
            toplace.AddRange(parentPlanet.ExpandFieldsFrom(5, f, 0.7f, 1.1f, 0.7f, closed, PLT_Utilities.PLT_ExpandConditions.expandForForest));

            for (int i = 0; i < toplace.Count; i++) {
                if (toplace[i].Prop != null) continue;
                toplace[i].AddProp(propDB.ForestTrees[parentPlanet.RNG.RandomRange(0, propDB.ForestTrees.Length)],0.8f,1.8f);
            }
        }
    }

    public void PopulatePropsBeach() {
        PLT_Field[] fields = parentPlanet.GetFieldData();

        for (int j = 0; j < propDB.BeachProps.Count; j++) {
            float propProbability = propDB.BeachProps[j].SpawnChancePerField;

            for (int i = 0; i < fields.Length; i++) {
                if (fields[i].FieldType == PLT_Biome.Sand && fields[i].Prop == null) {

                    float r = parentPlanet.RNG.RandomRange(0, 1f);
                    if (r < propProbability) {

                        fields[i].AddProp(propDB.BeachProps[j].SourcePrefab, 0.6f, 1.1f);
                    }

                }
            }
        }
    }

    public void PopulatePropsGrassland() {
        PLT_Field[] fields = parentPlanet.GetFieldData();

        for (int j = 0; j < propDB.GrassLandProps.Count; j++) {

            float propProbability = propDB.GrassLandProps[j].SpawnChancePerField;


            for (int i = 0; i < fields.Length; i++) {
                if (fields[i].FieldType == PLT_Biome.Grass && fields[i].Prop == null) {

                    float r = parentPlanet.RNG.RandomRange(0, 1f);
                    if (r < propProbability) {

                        fields[i].AddProp(propDB.GrassLandProps[j].SourcePrefab, 0.6f, 1.1f);

                    }

                }
            }
        }
    }

    public void PopulatePropsPolar() {
        PLT_Field[] fields = parentPlanet.GetFieldData();

        for (int j = 0; j < propDB.PolarProps.Count; j++) {

            float propProbability = propDB.PolarProps[j].SpawnChancePerField;

            for (int i = 0; i < fields.Length; i++) {
                if (fields[i].FieldType == PLT_Biome.PolarRegion && fields[i].Prop == null && fields[i].center.magnitude > PLT_Config.BIOME_GLOBAL_HEIGHT_SAND) {

                    float r = parentPlanet.RNG.RandomRange(0, 1f);
                    if (r < propProbability) {

                        fields[i].AddProp(propDB.PolarProps[j].SourcePrefab, 0.6f, 1.1f);
                    }

                }
            }

        }
    }

    public void PostProSpawnProps() {

        if(propDB == null) {
            propDB = GameObject.FindObjectOfType<PLT_PlanetPropDB>();
        }

        PopulatePropsForests();
        PopulatePropsBeach();
        PopulatePropsPolar();
        PopulatePropsGrassland();
  
    }

    void CreateContinent(PLT_Field start, int iterations, float acWeight, float abWeight, float bcWeight, ExpandCondition condition) {

        List<PLT_Field> ret = new List<PLT_Field>();
        List<PLT_Field> closed = new List<PLT_Field>();
        ret.Add(start);
        ret.AddRange(parentPlanet.ExpandFieldsFrom(iterations, start, acWeight, abWeight, bcWeight, closed, condition));
        MakeFieldsGrass(ret.ToArray());

    }

    public bool MakeFieldsDeepSea(PLT_Field[] fields) {

        PLT_Field f = null;
        bool change = false;
        for (int i = 0; i < fields.Length; i++) {
            f = fields[i];
            if (f.FieldType == PLT_Biome.DeepWater)
                continue;

            change = true;

            f.FieldType = PLT_Biome.DeepWater;

            parentPlanet.VertexDatabase[f.v1] = parentPlanet.VertexDatabase[f.v1].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;
            parentPlanet.VertexDatabase[f.v2] = parentPlanet.VertexDatabase[f.v2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;
            parentPlanet.VertexDatabase[f.v3] = parentPlanet.VertexDatabase[f.v3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;

            parentPlanet.VertexDatabase[f.wAB2] = parentPlanet.VertexDatabase[f.wAB2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;
            parentPlanet.VertexDatabase[f.wAB3] = parentPlanet.VertexDatabase[f.wAB3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;

            parentPlanet.VertexDatabase[f.wBC2] = parentPlanet.VertexDatabase[f.wBC2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;
            parentPlanet.VertexDatabase[f.wBC3] = parentPlanet.VertexDatabase[f.wBC3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;

            parentPlanet.VertexDatabase[f.wCA2] = parentPlanet.VertexDatabase[f.wCA2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;
            parentPlanet.VertexDatabase[f.wCA3] = parentPlanet.VertexDatabase[f.wCA3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_DEEPSEA;

            f.center = PLT_Utilities.GetMiddle(parentPlanet.VertexDatabase[f.v1], parentPlanet.VertexDatabase[f.v2], parentPlanet.VertexDatabase[f.v3]);


            parentPlanet.PlanetColorDatabase[f.v1] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.v2] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.v3] = PLT_Config.COLOR_DEEPSEA;

            parentPlanet.PlanetColorDatabase[f.wAB1] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wAB2] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wAB3] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wAB4] = PLT_Config.COLOR_DEEPSEA;

            parentPlanet.PlanetColorDatabase[f.wBC1] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wBC2] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wBC3] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wBC4] = PLT_Config.COLOR_DEEPSEA;

            parentPlanet.PlanetColorDatabase[f.wCA1] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wCA2] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wCA3] = PLT_Config.COLOR_DEEPSEA;
            parentPlanet.PlanetColorDatabase[f.wCA4] = PLT_Config.COLOR_DEEPSEA;

        }
        return change;
    }

    public bool MakeFieldsSea(PLT_Field[] fields) {
        PLT_Field f = null;

        bool change = false;

        for (int i = 0; i < fields.Length; i++) {

            f = fields[i];
            if (f.FieldType == PLT_Biome.Water)
                continue;

            change = true;
            f.FieldType = PLT_Biome.Water;

            parentPlanet.VertexDatabase[f.v1] = parentPlanet.VertexDatabase[f.v1].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;
            parentPlanet.VertexDatabase[f.v2] = parentPlanet.VertexDatabase[f.v2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;
            parentPlanet.VertexDatabase[f.v3] = parentPlanet.VertexDatabase[f.v3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;

            parentPlanet.VertexDatabase[f.wAB2] = parentPlanet.VertexDatabase[f.wAB2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;
            parentPlanet.VertexDatabase[f.wAB3] = parentPlanet.VertexDatabase[f.wAB3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;

            parentPlanet.VertexDatabase[f.wBC2] = parentPlanet.VertexDatabase[f.wBC2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;
            parentPlanet.VertexDatabase[f.wBC3] = parentPlanet.VertexDatabase[f.wBC3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;

            parentPlanet.VertexDatabase[f.wCA2] = parentPlanet.VertexDatabase[f.wCA2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;
            parentPlanet.VertexDatabase[f.wCA3] = parentPlanet.VertexDatabase[f.wCA3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SEA;


            f.center = PLT_Utilities.GetMiddle(parentPlanet.VertexDatabase[f.v1], parentPlanet.VertexDatabase[f.v2], parentPlanet.VertexDatabase[f.v3]);

            parentPlanet.PlanetColorDatabase[f.v1] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.v2] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.v3] = PLT_Config.COLOR_SEA;


            parentPlanet.PlanetColorDatabase[f.wAB1] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wAB2] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wAB3] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wAB4] = PLT_Config.COLOR_SEA;

            parentPlanet.PlanetColorDatabase[f.wBC1] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wBC2] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wBC3] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wBC4] = PLT_Config.COLOR_SEA;

            parentPlanet.PlanetColorDatabase[f.wCA1] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wCA2] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wCA3] = PLT_Config.COLOR_SEA;
            parentPlanet.PlanetColorDatabase[f.wCA4] = PLT_Config.COLOR_SEA;



        }
        return change;
    }

    public bool MakeFieldsSand(PLT_Field[] fields) {

        PLT_Field f = null;
        bool change = false;
        for (int i = 0; i < fields.Length; i++) {
            f = fields[i];
            if (f.FieldType == PLT_Biome.Sand)
                continue;

            change = true;

            f.FieldType = PLT_Biome.Sand;

            parentPlanet.VertexDatabase[f.v1] = parentPlanet.VertexDatabase[f.v1].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;
            parentPlanet.VertexDatabase[f.v2] = parentPlanet.VertexDatabase[f.v2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;
            parentPlanet.VertexDatabase[f.v3] = parentPlanet.VertexDatabase[f.v3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;

            parentPlanet.VertexDatabase[f.wAB2] = parentPlanet.VertexDatabase[f.wAB2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;
            parentPlanet.VertexDatabase[f.wAB3] = parentPlanet.VertexDatabase[f.wAB3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;

            parentPlanet.VertexDatabase[f.wBC2] = parentPlanet.VertexDatabase[f.wBC2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;
            parentPlanet.VertexDatabase[f.wBC3] = parentPlanet.VertexDatabase[f.wBC3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;

            parentPlanet.VertexDatabase[f.wCA2] = parentPlanet.VertexDatabase[f.wCA2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;
            parentPlanet.VertexDatabase[f.wCA3] = parentPlanet.VertexDatabase[f.wCA3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;

            f.center = PLT_Utilities.GetMiddle(parentPlanet.VertexDatabase[f.v1], parentPlanet.VertexDatabase[f.v2], parentPlanet.VertexDatabase[f.v3]);


            parentPlanet.PlanetColorDatabase[f.v1] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.v2] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.v3] = PLT_Config.COLOR_BEACH;

            parentPlanet.PlanetColorDatabase[f.wAB1] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wAB2] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wAB3] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wAB4] = PLT_Config.COLOR_BEACH;

            parentPlanet.PlanetColorDatabase[f.wBC1] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wBC2] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wBC3] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wBC4] = PLT_Config.COLOR_BEACH;

            parentPlanet.PlanetColorDatabase[f.wCA1] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wCA2] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wCA3] = PLT_Config.COLOR_BEACH;
            parentPlanet.PlanetColorDatabase[f.wCA4] = PLT_Config.COLOR_BEACH;

        }
        return change;

    }

    public bool MakeFieldsGrass(PLT_Field[] fields) {

        PLT_Field f = null;
        bool change = false;
        for (int i = 0; i < fields.Length; i++) {
            f = fields[i];
            if (f.FieldType == PLT_Biome.Grass)
                continue;

            change = true;

            f.FieldType = PLT_Biome.Grass;

            parentPlanet.VertexDatabase[f.v1] = parentPlanet.VertexDatabase[f.v1].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;
            parentPlanet.VertexDatabase[f.v2] = parentPlanet.VertexDatabase[f.v2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;
            parentPlanet.VertexDatabase[f.v3] = parentPlanet.VertexDatabase[f.v3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;

            parentPlanet.VertexDatabase[f.wAB2] = parentPlanet.VertexDatabase[f.wAB2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;
            parentPlanet.VertexDatabase[f.wAB3] = parentPlanet.VertexDatabase[f.wAB3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;

            parentPlanet.VertexDatabase[f.wBC2] = parentPlanet.VertexDatabase[f.wBC2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;
            parentPlanet.VertexDatabase[f.wBC3] = parentPlanet.VertexDatabase[f.wBC3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;

            parentPlanet.VertexDatabase[f.wCA2] = parentPlanet.VertexDatabase[f.wCA2].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;
            parentPlanet.VertexDatabase[f.wCA3] = parentPlanet.VertexDatabase[f.wCA3].normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS;

            f.center = PLT_Utilities.GetMiddle(parentPlanet.VertexDatabase[f.v1], parentPlanet.VertexDatabase[f.v2], parentPlanet.VertexDatabase[f.v3]);


            parentPlanet.PlanetColorDatabase[f.v1] = PLT_Config.COLOR_GRASS;
            parentPlanet.PlanetColorDatabase[f.v2] = PLT_Config.COLOR_GRASS;
            parentPlanet.PlanetColorDatabase[f.v3] = PLT_Config.COLOR_GRASS;

            parentPlanet.PlanetColorDatabase[f.wAB1] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wAB2] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wAB3] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wAB4] = PLT_Config.COLOR_MUD;

            parentPlanet.PlanetColorDatabase[f.wBC1] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wBC2] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wBC3] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wBC4] = PLT_Config.COLOR_MUD;

            parentPlanet.PlanetColorDatabase[f.wCA1] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wCA2] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wCA3] = PLT_Config.COLOR_MUD;
            parentPlanet.PlanetColorDatabase[f.wCA4] = PLT_Config.COLOR_MUD;

        }
        return change;

    }

    public void MakeFieldMountainCustomHeight(PLT_Field f, float heightOffset) {


        f.FieldType = PLT_Biome.HillsAndMountains;
        f.HeightOffset = heightOffset;

        Color c = PLT_Config.COLOR_SNOW;

        if (heightOffset < PLT_Config.BIOME_MOUNTAIN_HEIGHT_HILLS) {
            c = PLT_Config.COLOR_HILLS;
        } else if (heightOffset < PLT_Config.BIOME_MOUNTAIN_HEIGHT_BASE) {
            c = PLT_Config.COLOR_MOUNTAINBASE;
        } else if (heightOffset < PLT_Config.BIOME_MOUNTAIN_HEIGHT_LOW) {
            c = PLT_Config.COLOR_MOUNTAINLOW;
        } else if (heightOffset < PLT_Config.BIOME_MOUNTAIN_HEIGHT_HIGH) {
            c = PLT_Config.COLOR_MOUNTAINHIGH;
        } else {
            c = PLT_Config.COLOR_SNOW;
        }


        parentPlanet.VertexDatabase[f.v1] = parentPlanet.VertexDatabase[f.v1].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);
        parentPlanet.VertexDatabase[f.v2] = parentPlanet.VertexDatabase[f.v2].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);
        parentPlanet.VertexDatabase[f.v3] = parentPlanet.VertexDatabase[f.v3].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);

        parentPlanet.VertexDatabase[f.wAB2] = parentPlanet.VertexDatabase[f.wAB2].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);
        parentPlanet.VertexDatabase[f.wAB3] = parentPlanet.VertexDatabase[f.wAB3].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);

        parentPlanet.VertexDatabase[f.wBC2] = parentPlanet.VertexDatabase[f.wBC2].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);
        parentPlanet.VertexDatabase[f.wBC3] = parentPlanet.VertexDatabase[f.wBC3].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);

        parentPlanet.VertexDatabase[f.wCA2] = parentPlanet.VertexDatabase[f.wCA2].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);
        parentPlanet.VertexDatabase[f.wCA3] = parentPlanet.VertexDatabase[f.wCA3].normalized * (PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS + heightOffset);

        f.center = PLT_Utilities.GetMiddle(parentPlanet.VertexDatabase[f.v1], parentPlanet.VertexDatabase[f.v2], parentPlanet.VertexDatabase[f.v3]);


        parentPlanet.PlanetColorDatabase[f.v1] = c;
        parentPlanet.PlanetColorDatabase[f.v2] = c;
        parentPlanet.PlanetColorDatabase[f.v3] = c;

        parentPlanet.PlanetColorDatabase[f.wAB1] = c;
        parentPlanet.PlanetColorDatabase[f.wAB2] = c;
        parentPlanet.PlanetColorDatabase[f.wAB3] = c;
        parentPlanet.PlanetColorDatabase[f.wAB4] = c;

        parentPlanet.PlanetColorDatabase[f.wBC1] = c;
        parentPlanet.PlanetColorDatabase[f.wBC2] = c;
        parentPlanet.PlanetColorDatabase[f.wBC3] = c;
        parentPlanet.PlanetColorDatabase[f.wBC4] = c;

        parentPlanet.PlanetColorDatabase[f.wCA1] = c;
        parentPlanet.PlanetColorDatabase[f.wCA2] = c;
        parentPlanet.PlanetColorDatabase[f.wCA3] = c;
        parentPlanet.PlanetColorDatabase[f.wCA4] = c;
    }

    /// <summary>
    /// Makes a field into a polar region, will fail with SEA fields that are too far away from the pole
    /// </summary>
    /// <param name="f"></param>
    /// <param name="distanceToPole"></param>
    public void MakeFieldPolarRegion(PLT_Field f, float distanceToPole) {

        if (f.FieldType == PLT_Biome.DeepWater || f.FieldType == PLT_Biome.Water) {
            if (distanceToPole > 0.25f)
                return;
        }

        f.FieldType = PLT_Biome.PolarRegion;

        

        Color c = PLT_Config.COLOR_SNOW;

        float heightOffset = Mathf.Abs(f.center.magnitude - PLT_Config.BIOME_GLOBAL_HEIGHT_GRASS);

        if (heightOffset > .3f) {
            c = PLT_Config.COLOR_MOUNTAINHIGH;
        }

        //c = Color.Lerp(c,PLT_Config.COLOR_MountainBase, distanceToPole*.7f);


        parentPlanet.PlanetColorDatabase[f.v1] = c;
        parentPlanet.PlanetColorDatabase[f.v2] = c;
        parentPlanet.PlanetColorDatabase[f.v3] = c;

        parentPlanet.PlanetColorDatabase[f.wAB1] = c;
        parentPlanet.PlanetColorDatabase[f.wAB2] = c;
        parentPlanet.PlanetColorDatabase[f.wAB3] = c;
        parentPlanet.PlanetColorDatabase[f.wAB4] = c;

        parentPlanet.PlanetColorDatabase[f.wBC1] = c;
        parentPlanet.PlanetColorDatabase[f.wBC2] = c;
        parentPlanet.PlanetColorDatabase[f.wBC3] = c;
        parentPlanet.PlanetColorDatabase[f.wBC4] = c;

        parentPlanet.PlanetColorDatabase[f.wCA1] = c;
        parentPlanet.PlanetColorDatabase[f.wCA2] = c;
        parentPlanet.PlanetColorDatabase[f.wCA3] = c;
        parentPlanet.PlanetColorDatabase[f.wCA4] = c;
    }

    #region expand conditions
    


    #endregion

}
