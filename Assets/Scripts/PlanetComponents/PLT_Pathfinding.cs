﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public struct PLT_PathfindingResult {
    public PLT_Field Origin;
    public PLT_Field Destination;
    public Vector3[] WalkPathData;
    public PLT_Field[] RawPathData;
}
public class PLT_Pathfinding : MonoBehaviour {

    public delegate Vector3[] PathSmoothingFunction(PLT_Field[] rawPathData);

    PLT_Planet m_parentPlanet;
    PLT_PathfindingField[] m_pathfindingData;


    public class PathSmoothingFunctions {

        private static Vector3 GetPositionFromField(PLT_Field f) {
            Vector3 pos = f.center;
            if(f.FieldType == PLT_Biome.DeepWater || f.FieldType == PLT_Biome.Water) {
                pos = f.center.normalized * PLT_Config.BIOME_GLOBAL_HEIGHT_SAND;
            }
            return pos;
        }

        /// <summary>
        /// Smooths the Path to a Vector3 array by averaging close positions
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Vector3[] AverageSmooth(PLT_Field[] rawPath) {
            if (rawPath == null)
                return null;
            if (rawPath.Length == 0)
                return null;
            if(rawPath.Length == 1) {
                Vector3[] pos = new Vector3[1];
                pos[0] = GetPositionFromField(rawPath[0]);
                return pos;
            }

            List<Vector3> positions = new List<Vector3>();
            for (int i = 0; i < rawPath.Length-1; i++) {
                Vector3 posOne = GetPositionFromField(rawPath[i]);
                Vector3 posTwo = GetPositionFromField(rawPath[i+1]);
                positions.Add((posOne + posTwo) * .5f);
            }
            return positions.ToArray();
        }
    }


    public void Initialize(PLT_Planet parent) {
        m_parentPlanet = parent;
        UpdatePathFindingData();
    }

    private PLT_PathfindingField GetPathfindingFieldFromField(PLT_Field f) {
        for (int i = 0; i < m_pathfindingData.Length; i++) {
            if (m_pathfindingData[i].IsField(f)) {
                return m_pathfindingData[i];
            }
        }
        return null;
    }

    /// <summary>
    /// Rudimentary Implementation of the A* Algorithm
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="goal"></param>
    /// <returns></returns>
    private PLT_Field[] CalculateRawPathFromTo(PLT_Field origin, PLT_Field goal) {

        List<PLT_PathfindingField> OPEN = new List<PLT_PathfindingField>();
        List<PLT_PathfindingField> CLOSED = new List<PLT_PathfindingField>();

        PLT_PathfindingField _origin = GetPathfindingFieldFromField(origin);
        PLT_PathfindingField _goal = GetPathfindingFieldFromField(goal);

        // opt Case, _goal  or origin is blocked, return null
        if (_origin.IsBlocked() || _goal.IsBlocked())
            return null;

        OPEN.Add(_origin);

        while (OPEN.Count != 0) {

            // find node with lowest TotalCost
            float lowest = float.MaxValue;
            PLT_PathfindingField currentNode = null;
            for (int i = 0; i < OPEN.Count; i++) {
                if (OPEN[i].f < lowest) {
                    currentNode = OPEN[i];
                    lowest = currentNode.f;
                }
            }

            /*** STOP THE SEARCH HERE, Found Goal ****/
            if (currentNode == _goal) {
                List<PLT_Field> path = new List<PLT_Field>();
                PLT_PathfindingField c = _goal;
                while (c != null) {
             
                    path.Add(c.GetActualPLTField());
                    c = c.CurrentParentField;
 
                }

                CleanUpPathfinding();

                // reverse the path to proper order and return
                path.Reverse();
                return path.ToArray();

            }


            // Remove node from the open list
            OPEN.Remove(currentNode);
            CLOSED.Add(currentNode);

            // for all neighbours
            for (int i = 0; i < currentNode.Neighbours.Length; i++) {

                // Check if field is traversable
                if (currentNode.Neighbours[i].IsBlocked())
                    continue;



                bool clContains = CLOSED.Contains(currentNode.Neighbours[i]);
                bool opContains = OPEN.Contains(currentNode.Neighbours[i]);
                // if this node has already been closed
                if (clContains && currentNode.g < currentNode.Neighbours[i].g) {


                    currentNode.Neighbours[i].g = currentNode.g + currentNode.TraversalCost;
                    // Todo: Squared direct line is probably a really bad heuristic for a spherical, 3 directional A*. Think of something better in the future
                    currentNode.Neighbours[i].h = Vector3.SqrMagnitude(_goal.GetCenterPosition() - currentNode.Neighbours[i].GetCenterPosition());
                    currentNode.Neighbours[i].f = currentNode.Neighbours[i].g + currentNode.Neighbours[i].h;
                    currentNode.Neighbours[i].CurrentParentField = currentNode;

                } else if (opContains && currentNode.g < currentNode.Neighbours[i].g) {


                    currentNode.Neighbours[i].g = currentNode.g + currentNode.TraversalCost;
                    // Todo: Squared direct line is probably a really bad heuristic for a spherical, 3 directional A*. Think of something better in the future
                    currentNode.Neighbours[i].h = Vector3.SqrMagnitude(_goal.GetCenterPosition() - currentNode.Neighbours[i].GetCenterPosition());
                    currentNode.Neighbours[i].f = currentNode.Neighbours[i].g + currentNode.Neighbours[i].h;
                    currentNode.Neighbours[i].CurrentParentField = currentNode;

                } else if (!clContains && !opContains) {

                    currentNode.Neighbours[i].g = currentNode.g + currentNode.TraversalCost;
                    // Todo: Squared direct line is probably a really bad heuristic for a spherical, 3 directional A*. Think of something better in the future
                    currentNode.Neighbours[i].h = Vector3.SqrMagnitude(_goal.GetCenterPosition() - currentNode.Neighbours[i].GetCenterPosition());
                    currentNode.Neighbours[i].f = currentNode.Neighbours[i].g + currentNode.Neighbours[i].h;

                    currentNode.Neighbours[i].CurrentParentField = currentNode;
                    OPEN.Add(currentNode.Neighbours[i]);
                }
            }

           

        }
        // NO PATH FOUND
        CleanUpPathfinding();
        return null;

    }

    /// <summary>
    /// Finds a Path from to the specified points and postprocesses it with a smoothing function as defined in
    /// PLT_Pathfinding.PathSmoothingFunctions
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="smoothingFunction"></param>
    /// <returns></returns>
    public PLT_PathfindingResult FindPath(PLT_Field from, PLT_Field to, PathSmoothingFunction smoothingFunction) {

        PLT_PathfindingResult result;


        result.Origin = from;
        result.Destination = to;
        result.RawPathData = CalculateRawPathFromTo(from, to);
        result.WalkPathData = smoothingFunction(result.RawPathData);

        return result;

    }


    public void UpdatePathFindingData() {

        // Build Pathfinding Data for this planet
        PLT_Field[] fields = m_parentPlanet.GetFieldData();
        m_pathfindingData = new PLT_PathfindingField[fields.Length];

        for (int i = 0; i < fields.Length; i++) {

            float cost = 0;
            if (fields[i].FieldType == PLT_Biome.DeepWater) {
                cost = PLT_Config.PATHFINDING_COST_DEEPSEA;
            } else if (fields[i].FieldType == PLT_Biome.Water) {
                cost = PLT_Config.PATHFINDING_COST_SEA;
            } else if (fields[i].FieldType == PLT_Biome.Grass) {
                cost = PLT_Config.PATHFINDING_COST_GRASS;
            } else if (fields[i].FieldType == PLT_Biome.Sand) {
                cost = PLT_Config.PATHFINDING_COST_BEACH;
            } else if (fields[i].FieldType == PLT_Biome.HillsAndMountains) {
                if (fields[i].HeightOffset < PLT_Config.BIOME_MOUNTAIN_HEIGHT_LOW) {
                    cost = PLT_Config.PATHFINDING_COST_HILLS;
                } else {
                    cost = PLT_Config.PATHFINDING_COST_MOUNTAINS;
                }
            }


            m_pathfindingData[i] = new PLT_PathfindingField(fields[i], cost);
        }

        for (int i = 0; i < m_pathfindingData.Length; i++) {
            m_pathfindingData[i].ConnectToNeighbours(m_pathfindingData);
        }
    }

    private void CleanUpPathfinding() {
        for (int i = 0; i < m_pathfindingData.Length; i++) {
            m_pathfindingData[i].g = 0;
            m_pathfindingData[i].h = 0;
            m_pathfindingData[i].f = 0;
            m_pathfindingData[i].CurrentParentField = null;

        }
    }


}
