﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PLT_Factions : MonoBehaviour {

    PLT_Planet parentPlanet;
    PLT_PlanetPropDB propDB;

    public void Initialize(PLT_Planet parentPlanet) {
        this.parentPlanet = parentPlanet;
        propDB = GameObject.FindObjectOfType<PLT_PlanetPropDB>();
    }

    public void SpawnCity() {

        PLT_Field[] fdata = parentPlanet.GetFieldData();

       


        // prepare loop for n cities
        int cityCount = 3;
        List<PLT_Field> existingCities = new List<PLT_Field>();
        for (int u = 0; u < cityCount; u++) {

            // Find all fields that are eligible for a castle
            // 1. Grassland empty of props
            // 2. Needs to be a big tri (field and all of its neighbours same fieldtype empty of props)
            List<PLT_Field> eligibleForCastle = new List<PLT_Field>();
            for (int i = 0; i < fdata.Length; i++) {
                PLT_Field f = fdata[i];
                if (f.FieldType != PLT_Biome.Grass || f.Prop != null)
                    continue;
                if (f.AdjacentAB.FieldType != PLT_Biome.Grass || f.AdjacentAB.Prop != null)
                    continue;
                if (f.AdjacentBC.FieldType != PLT_Biome.Grass || f.AdjacentBC.Prop != null)
                    continue;
                if (f.AdjacentAC.FieldType != PLT_Biome.Grass || f.AdjacentAC.Prop != null)
                    continue;
                eligibleForCastle.Add(f);
            }

            PLT_Field cityCenter;
            // make sure the cities are farthest apart as possible
            // if this is the first city, pick an eligible field at random
            if (existingCities.Count == 0) {

                 cityCenter = eligibleForCastle[parentPlanet.RNG.RandomRange(0, eligibleForCastle.Count)];

            } 
            // Otherwise get a field that is farthest away from the median of all cities so we get a nice distribution
            else {
                float maxDist = 0;
                PLT_Field farthestField = eligibleForCastle[0];
                Vector3 cityaverage = Vector3.zero ;
                for (int i = 0; i < existingCities.Count; i++) {
                    cityaverage += existingCities[i].center;
                }
                cityaverage /= existingCities.Count;

                for (int i = 0; i < eligibleForCastle.Count; i++) {
                    float dist = Vector3.SqrMagnitude(eligibleForCastle[i].center - cityaverage);
                    if (dist > maxDist) {
                        maxDist = dist;
                        farthestField = eligibleForCastle[i];
                    }
                }

                cityCenter = farthestField;
            }

            // place a castle on this field, reference the prop on the other fields too 
            GameObject addedProp = cityCenter.AddProp(propDB.Castle, 0.6f, 1.1f);
           
            cityCenter.AdjacentAB.Prop = addedProp;
            cityCenter.AdjacentAC.Prop = addedProp;
            cityCenter.AdjacentBC.Prop = addedProp;

            // Now expand the city with houses around it
            List<PLT_Field> cityproper = new List<PLT_Field>();
            List<PLT_Field> closed = new List<PLT_Field>();
            cityproper.AddRange(parentPlanet.ExpandFieldsFrom(7, cityCenter, 0.9f, 0.9f, 0.9f, closed, PLT_Utilities.PLT_ExpandConditions.expandAlways));

            for (int i = 0; i < cityproper.Count; i++) {
                PLT_Field f = cityproper[i];
                if (f.Prop != null || f.FieldType == PLT_Biome.DeepWater || f.FieldType == PLT_Biome.Water) continue;

                float chance = parentPlanet.RNG.RandomRange(0f, 1f);
                // spawn a home?
                if (chance < 0.9f) {
                    f.AddProp(propDB.CityHouse, 0.6f, 1.1f); 
                }
            }
        }


    }

}
