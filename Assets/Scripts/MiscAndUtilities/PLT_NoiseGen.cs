﻿using System.Collections;

using UnityEngine;

public class PLT_NoiseGen {

    public string Seed { get; set; }
    public float Persistance { get; set; }
    public int Octaves { get; set; }

    int RandomNumberCountGenCount;

    #region Interpolation


    float linear_interpolate(float lhs, float rhs, float t) {
        return lhs + (rhs - lhs) * t;
    }
    float cosine_interpolate(float lhs, float rhs, float t) {
        float timeRadial = t * Mathf.PI;

        float trCos = (1 - Mathf.Cos(timeRadial)) * 0.5f;

        return lhs * (1 - trCos) + rhs * trCos;

    }


    float cubic_interpolate(float v0, float v1, float v2, float v3, float t) {
        float P = (v3 - v2) - (v0 - v1);
        float Q = (v0 - v1) - P;
        float R = v2 - v0;
        float S = v1;

        return P * Mathf.Pow(t, 3) + Q * Mathf.Pow(t, 2) + R * t + S;
    }

    #endregion

    public float RandomRange(float min, float max) {
        RandomNumberCountGenCount++;
        if (min == max)
            return min;
        float f = Noise(RandomNumberCountGenCount);// + 2)*0.5f;
        f = ((max - min) * 0.5f) * f + ((max + min) * 0.5f);
        return f;
        
    }

    public int RandomRange(int min, int max) {
        RandomNumberCountGenCount++;
        if (min == max)
            return min;
        float f = Noise(RandomNumberCountGenCount);// + 2)*0.5f;
        f = ((max - min) * 0.5f) * f + ((max + min) * 0.5f);

        return (int)f;
    }

    #region Noise Methods
    /// <summary>
    /// Returns a floating point number between -1.0 and 1.0
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public float Noise(int x) {

        x = x + Seed[Mathf.Abs((Mathf.Abs((x + 1) % Seed.Length)))];
        x = (x << 13) ^ x;
        return (1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824f);
    }


    float Noise2D(int x, int y) {
        //Debug.Log(((Mathf.Abs(x) + 1) * (Mathf.Abs(y) + 1)) % (seed.Length));
        int c = (x + y * 17) * Seed[Mathf.Abs(((x + 57) * (y + 57))) % (Seed.Length - 1)];
        c = (c << 13) ^ c;
        return (1.0f - ((c * (c * c * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824f);
    }

    float Noise3D(int x, int y, int z) {
        //Debug.Log(((Mathf.Abs(x) + 1) * (Mathf.Abs(y) + 1)) % (seed.Length));
        int c = (x + y + z * 17) * Seed[Mathf.Abs(((x + 57) * (y + 57) * (z + 57))) % (Seed.Length - 1)];
        c = (c << 13) ^ c;
        return (1.0f - ((c * (c * c * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824f);
    }

    #endregion

    # region 1D Perlin Noise

    public float SmoothedNoise(int x, int smoothcount) {
        return (Noise(x - 1) / (4 * smoothcount)) + Noise(x) / (2 * smoothcount) + (Noise(x + 1) / (4 * smoothcount));

    }

    public float InterpolatedNoise(float x, int smoothcount) {
        int lhs = (int)x;
        int rhs = (int)x + 1;

        float delta = x - lhs;

        float smoothLLhs = SmoothedNoise(lhs - 1, smoothcount);
        float smoothLhs = SmoothedNoise(lhs, smoothcount);
        float smoothRhs = SmoothedNoise(rhs, smoothcount);
        float smoothRRhs = SmoothedNoise(rhs + 1, smoothcount);

        return (cubic_interpolate(smoothLLhs, smoothLhs, smoothRhs, smoothRRhs, delta));
    }


    public float PerlinNoise1D(float x, int smoothcount = 1) {

        float y = 0;



        for (int i = 0; i < Octaves; i++) {
            float frequency = Mathf.Pow(2, i);
            float amplitude = Mathf.Pow(Persistance, i);

            y += InterpolatedNoise(x * frequency, smoothcount) * amplitude;
        }

        return y;
    }

    #endregion

    #region 2D Perlin Noise

    public float Smoothed2DNoise(int x, int y, int smoothcount = 1) {
        float corners = (Noise2D(x - 1, y - 1) +
                         Noise2D(x - 1, y + 1) +
                         Noise2D(x + 1, y + 1) +
                         Noise2D(x + 1, y - 1))
                         / (8 * smoothcount);

        float sides = (Noise2D(x - 1, y) +
                       Noise2D(x + 1, y) +
                       Noise2D(x, y + 1) +
                       Noise2D(x, y - 1))
                       / (4 * smoothcount);

        float center = Noise2D(x, y) / (2 * smoothcount);

        return corners + sides + center;
    }

    public float Interpolated2DNoise(float x, float y, int smoothcount = 1) {
        int lhsX = (int)x;
        int rhsX = (int)x + 1;
        float deltaX = x - lhsX;

        int lhsY = (int)y;
        int rhsY = (int)y + 1;
        float deltaY = y - lhsY;


        // Calculate smoothed values for 4 points
        float origin = Smoothed2DNoise(lhsX, lhsY, smoothcount);
        float right = Smoothed2DNoise(rhsX, lhsY, smoothcount);
        float top = Smoothed2DNoise(lhsX, rhsY, smoothcount);
        float topright = Smoothed2DNoise(rhsX, rhsY, smoothcount);

        // Calculate two points on the parallel lines
        float origToRight = cosine_interpolate(origin, right, deltaX);
        float topToTopright = cosine_interpolate(top, topright, deltaX);

        // calculate point between those two points
        return cosine_interpolate(origToRight, topToTopright, deltaY);

    }

    public float PerlinNoise2D(float x, float y, int smoothCount = 1) {
        float z = 0;

        for (int i = 0; i < Octaves - 1; i++) {
            float frequency = Mathf.Pow(2, i);
            float amplitude = Mathf.Pow(Persistance, i);

            z += Interpolated2DNoise(x * frequency, y * frequency) * amplitude;
        }

        return z;

    }

    #endregion

    #region 3D Perlin Noise

    public float Smoothed3DNoise(int x, int y, int z, int smoothCount = 1) {
        float corners = (Noise3D(x - 1, y - 1, z - 1) +
                         Noise3D(x - 1, y - 1, z + 1) +
                         Noise3D(x - 1, y + 1, z - 1) +
                         Noise3D(x - 1, y + 1, z + 1) +
                         Noise3D(x + 1, y - 1, z - 1) +
                         Noise3D(x + 1, y - 1, z + 1) +
                         Noise3D(x + 1, y + 1, z - 1) +
                         Noise3D(x + 1, y + 1, z + 1)

                         )
                         / (32 * smoothCount);

        float sides = (Noise3D(x - 1, y, z) +
                       Noise3D(x + 1, y, z) +
                       Noise3D(x, y + 1, z) +
                       Noise3D(x, y - 1, z) +
                       Noise3D(x, y, z + 1) +
                       Noise3D(x, y, z - 1)

                       )
                       / (16 * smoothCount);

        float center = Noise3D(x, y, z) / (4 * smoothCount);

        return corners + sides + center;
    }

    public float Interpolated3DNoise(float x, float y, float z, int smoothcount = 1) {
        int lhsX = (int)x;
        int rhsX = (int)x + 1;
        float deltaX = x - lhsX;

        int lhsY = (int)y;
        int rhsY = (int)y + 1;
        float deltaY = y - lhsY;

        int lhsZ = (int)z;
        int rhsZ = (int)z + 1;
        float deltaZ = z - lhsZ;



        float leftbottom = Smoothed3DNoise(lhsX, lhsY, lhsZ, smoothcount);
        float leftbottombehind = Smoothed3DNoise(lhsX, lhsY, rhsZ, smoothcount);

        float lefttop = Smoothed3DNoise(lhsX, rhsY, lhsZ, smoothcount);
        float lefttopbehind = Smoothed3DNoise(lhsX, rhsY, rhsZ, smoothcount);

        float rightbottom = Smoothed3DNoise(rhsX, lhsY, lhsZ, smoothcount);
        float rightbottombehind = Smoothed3DNoise(rhsX, lhsY, rhsZ, smoothcount);

        float righttop = Smoothed3DNoise(rhsX, rhsY, lhsZ, smoothcount);
        float righttopbehind = Smoothed3DNoise(rhsX, rhsY, rhsZ, smoothcount);


        float leftbottomToBehind = linear_interpolate(leftbottom, leftbottombehind, deltaZ);
        float lefttopToBehind = linear_interpolate(lefttop, lefttopbehind, deltaZ);

        float rightbottomToBehind = linear_interpolate(rightbottom, rightbottombehind, deltaZ);
        float righttopToBehind = linear_interpolate(righttop, righttopbehind, deltaZ);



        float leftMiddle = linear_interpolate(leftbottomToBehind, lefttopToBehind, deltaY);
        float rightMiddle = linear_interpolate(rightbottomToBehind, righttopToBehind, deltaY);

        float smoothCenter = linear_interpolate(leftMiddle, rightMiddle, deltaX);

        // calculate point between those two points
        return smoothCenter;

    }

    public float PerlinNoise3D(float x, float y, float z, int smoothCount = 1) {
        float value = 0;

        for (int i = 0; i < Octaves - 1; i++) {
            float frequency = Mathf.Pow(2, i);
            float amplitude = Mathf.Pow(Persistance, i);

            value += Interpolated3DNoise(x * frequency, y * frequency, z * frequency) * amplitude;
        }

        return value;

    }
    #endregion

    #region 3D Perlin Noise Fast
    //http://paulbourke.net/miscellaneous/interpolation/
    public float TrilinearInterpolation(float c000, float c100, float c010, float c110, float c001, float c101, float c011, float c111, float x, float y, float z) {
        return (
            c000 * (1 - x) * (1 - y) * (1 - z) +
            c100 * x * (1 - y) * (1 - z) +
            c010 * (1 - x) * y * (1 - z) +
            c001 * (1 - x) * (1 - y) * z +
            c101 * x * (1 - y) * z +
            c011 * (1 - x) * y * z +
            c110 * x * y * (1 - z) +
            c111 * x * y * z
        );
    }

    public float Interpolated3DNoiseFast(float x, float y, float z) {
        // fastfloor function essentially. calculate closest integer but if it's negative subtract one so we floor it to the lower integer
        int x0 = (x > 0 ? (int)x : (int)x - 1);
        int y0 = (y > 0 ? (int)y : (int)y - 1);
        int z0 = (z > 0 ? (int)z : (int)z - 1);

        // preserve the decimals number behind the comma for later interpolation along this axis
        float restX = x - (float)x0;
        float restY = y - (float)y0;
        float restZ = z - (float)z0;

        // calculate 8 cube corners around our coordinate for interpolation

        float c000 = Noise3D(x0, y0, z0);
        float c100 = Noise3D(x0 + 1, y0, z0);
        float c010 = Noise3D(x0, y0 + 1, z0);
        float c110 = Noise3D(x0 + 1, y0 + 1, z0);
        float c001 = Noise3D(x0, y0, z0 + 1);
        float c101 = Noise3D(x0 + 1, y0, z0 + 1);
        float c011 = Noise3D(x0, y0 + 1, z0 + 1);
        float c111 = Noise3D(x0 + 1, y0 + 1, z0 + 1);

        return TrilinearInterpolation(c000, c100, c010, c110, c001, c101, c011, c111, restX, restY, restZ);

        // interpolate 
        // return (float)triLinearInterpolation(c000, c100, c010, c110, c001, c101, c011, c111, restX, restY, restZ);
    }

    public float PerlinNoise3DFast(float x, float y, float z) {
        float val = 0;
        float frequency = 1.0f;
        float curPersistance = 1.0f;
        for (int i = 0; i < Octaves; i++) {
            val += curPersistance * Interpolated3DNoiseFast(x * frequency, y * frequency, z * frequency);
            frequency *= 2;
            curPersistance *= Persistance;
        }
        return val;
    }

    #endregion

    #region Simplex Noise

    //public float SimplexNoise3D(float x, float y, float z) {
    //    return SimplexNoise.Noise.Generate(x, y, z);
    //}



    #endregion




}

