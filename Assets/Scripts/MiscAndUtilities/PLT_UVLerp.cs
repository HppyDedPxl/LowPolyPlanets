﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PLT_UVLerp : MonoBehaviour {

    Material m;
    float h;
    float v;
    float height;
    float counter;

    public float HSpeed;
    public float VSpeed;
    public float minHeight;
    public float maxHeight;
    public float heightSpeed;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
     //   if (m == null) {
     //       Renderer r = GetComponent<Renderer>();
     //       m = r.sharedMaterial;
     //   }

     //   h += 0.5f * Time.deltaTime * HSpeed;
     //   if (h > 1)
     //       h -= 2;
        


     //   v += 0.2f * Time.deltaTime * VSpeed;
     //   if (v > 1)
     //       v -= 2;

     // //  m.SetTextureOffset("_MainTex", new Vector2(h/2, v/2));
     ////   m.SetTextureOffset("_NoiseWaterMap",new Vector2(h,v));
     //   counter += Time.deltaTime * heightSpeed;
     //   float height = SineOscillation(counter, 1, minHeight, maxHeight);
     //   m.SetFloat("_WaveHeight", height);


    }

    float SineOscillation(float t, float p, float n, float m) {

        return (((m - n) * 0.5f) * Mathf.Sin(t * 2.0f * Mathf.PI * (1.0f / p))) + ((m + n) * 0.5f);

    }
}
