﻿using UnityEngine;
using System.Collections;

public class PLT_Camera : MonoBehaviour {

    public float MaxZoom;
    public float MinZoom;
    public AnimationCurve rotation;
    public AnimationCurve upwardsslope;

    float currentZoom = 0;

    void Update() {

        if (Input.mouseScrollDelta.y < 0) {
            currentZoom -= 2f * Time.deltaTime;
        }
        else if(Input.mouseScrollDelta.y > 0) {
            currentZoom += 2f * Time.deltaTime;
        }
        currentZoom = Mathf.Clamp(currentZoom, 0, 1);

        Vector3 CameraPos = new Vector3();

        CameraPos.z = Mathf.Lerp(MinZoom, MaxZoom, currentZoom);
        CameraPos.y = upwardsslope.Evaluate(currentZoom);

        Quaternion CameraRotation = Quaternion.Euler(rotation.Evaluate(currentZoom), 0, 0);

        transform.position = Vector3.Lerp(transform.position,CameraPos,0.2f);
        transform.rotation = Quaternion.Slerp(transform.rotation,CameraRotation,0.2f);

    }
}
