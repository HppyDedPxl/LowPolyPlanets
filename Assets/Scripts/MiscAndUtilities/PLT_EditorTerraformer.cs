﻿using UnityEngine;
using System.Collections;
using EasyEditor;
using System;

public class PLT_EditorTerraformer : MonoBehaviour {

    public string Seed = "Buttergolem";
    public float Resolution = 1;
    public float DeepSeaThreshold = -0.20f;
    public float SeaThreshold = 0f;
    public float SandThreshold = 0.1f;
    public float GrassThreshold = 0.7f;
    public float MountainBaseThreshold = 1f;
    public float MountainLowThreshold =1f;
    public float MountainHighThreshold = 1f;

    PLT_Terraformer terraformScript;
    PLT_Factions factionScript;

    [Inspector]
    public void Reterraform() {
        if (terraformScript == null) {
            terraformScript = GetComponent<PLT_Terraformer>();

        }
        terraformScript.ReTerraformWithNoise(Seed, Resolution, DeepSeaThreshold, SeaThreshold, SandThreshold, GrassThreshold, MountainBaseThreshold, MountainLowThreshold, MountainHighThreshold);
    }
    
    [Inspector]
    public void PlaceProps() {
        if (terraformScript == null) {
            terraformScript = GetComponent<PLT_Terraformer>();

        }
        terraformScript.PostProSpawnProps();
    }

    [Inspector]
    public void PlaceFactions() {
        if (factionScript == null) {
            factionScript = GetComponent<PLT_Factions>();
        }
        factionScript.SpawnCity();
    }
}
