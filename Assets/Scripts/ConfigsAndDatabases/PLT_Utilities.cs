﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class PLT_Utilities {

    public class PLT_ExpandConditions {

        public static bool expandIfSameTypeAndNoProp(PLT_Field lhs, PLT_Field rhs) {
            return ((lhs.FieldType == rhs.FieldType) && (rhs.Prop == null));
        }

        public static bool expandAlways(PLT_Field lhs, PLT_Field rhs) {
            return true;
        }

        public static bool expandPolar(PLT_Field lhs, PLT_Field rhs) {
            if (rhs.FieldType == PLT_Biome.Water || rhs.FieldType == PLT_Biome.DeepWater)
                return true;
            return false;
        }

        /// <summary>
        /// Allows expanding PLT_Fields if all of the following conditions are met:
        /// 1. Same Biome
        /// 2. No Props on either
        /// 3. Height Offset of said field is LEqual to PLT_Config.BIOME_MOUNTAIN_HEIGHT_HILLS
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static bool expandForForest(PLT_Field lhs, PLT_Field rhs) {
            return (
                (lhs.FieldType == rhs.FieldType) 
                && (rhs.Prop == null) 
                && (rhs.HeightOffset <= PLT_Config.BIOME_MOUNTAIN_HEIGHT_HILLS)
            );

        }
    }

    public static Vector3 GetMiddle(params Vector3[] v) {
        Vector3 sum = Vector3.zero;
        for (int i = 0; i < v.Length; i++) {
            sum += v[i];
        }
        return sum / v.Length;
    }

}

[Serializable]
public class PLT_Face {
    // Base Face
    public int v1, v2, v3;

    // Face walls
    public int wAB1, wAB2, wAB3, wAB4, wBC1, wBC2, wBC3, wBC4, wCA1, wCA2, wCA3, wCA4;

    public Vector3 center;
    public PLT_Face NeighbourAB;
    public PLT_Face NeighbourAC;
    public PLT_Face NeighbourCB;

    public PLT_Field ReferencedField;

    private PLT_Face() {

    }

    public PLT_Face(int v1, int v2, int v3, int wab1, int wab2, int wab3, int wab4, int wbc1, int wbc2, int wbc3, int wbc4, int wca1, int wca2, int wca3, int wca4, Vector3 center) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;

        // the side walls for scaling purposes
        this.wCA1 = wca1;
        this.wCA2 = wca2;
        this.wCA3 = wca3;
        this.wCA4 = wca4;

        this.wAB1 = wab1;
        this.wAB2 = wab2;
        this.wAB3 = wab3;
        this.wAB4 = wab4;

        this.wBC1 = wbc1;
        this.wBC2 = wbc2;
        this.wBC3 = wbc3;
        this.wBC4 = wbc4;

        this.center = center;
    }
}

[Serializable]
public class PLT_Field : PLT_Face {

    public PLT_Biome FieldType;

    public PLT_Planet ParentPlanet;
    public float HeightOffset;

    public PLT_Field AdjacentAC;
    public PLT_Field AdjacentAB;
    public PLT_Field AdjacentBC;



    public GameObject Prop;

    public PLT_Field(PLT_Face face, PLT_Biome FieldType, PLT_Planet parentPlanet) :
        base(face.v1, face.v2, face.v3,
             face.wAB1, face.wAB2, face.wAB3, face.wAB4,
             face.wBC1, face.wBC2, face.wBC3, face.wBC4,
             face.wCA1, face.wCA2, face.wCA3, face.wCA4, face.center) {

        this.FieldType = FieldType;
        NeighbourAB = face.NeighbourAB;
        NeighbourAC = face.NeighbourAC;
        NeighbourCB = face.NeighbourCB;

        ParentPlanet = parentPlanet;
    }

    public void ConnectFields() {
        AdjacentBC = NeighbourCB.ReferencedField;
        AdjacentAC = NeighbourAC.ReferencedField;
        AdjacentAB = NeighbourAB.ReferencedField;
    }

    public GameObject AddProp(GameObject prefab, float minSize, float maxSize) {

        GameObject prop = GameObject.Instantiate(prefab);
        Prop = prop;

        Vector3 v = ParentPlanet.LocalToWorldPoint(center);
        prop.transform.parent = ParentPlanet.transform;
        prop.transform.localPosition = center;
        float scale = ParentPlanet.RNG.RandomRange(minSize, maxSize);
        prop.transform.localScale = new Vector3(prop.transform.localScale.x * scale, prop.transform.localScale.y * scale, prop.transform.localScale.z * scale);
        prop.transform.Rotate(prop.transform.forward, ParentPlanet.RNG.RandomRange(0, 360));
        prop.transform.LookAt(v + v);

        return prop;

    }

    public PLT_Tuple<List<Vector3>, List<Color>>  GetFaces(ref Vector3[] VertexDatabase, ref Color[] ColorDatabase) {

        List<Vector3> verts = new List<Vector3>();
        List<Color> cols = new List<Color>();

        verts.Add(VertexDatabase[v1]);
        cols.Add(ColorDatabase[v1]);
        verts.Add(VertexDatabase[v2]);
        cols.Add(ColorDatabase[v2]);
        verts.Add(VertexDatabase[v3]);
        cols.Add(ColorDatabase[v3]);

        int fadded = 0;

        // either there is nothing or a different field type next to this
        if ((AdjacentBC == null || AdjacentBC.FieldType != FieldType) || 
            // or there is something there, we are mountains and are higher than the other thing.
            (AdjacentBC != null && (FieldType == PLT_Biome.HillsAndMountains || FieldType == PLT_Biome.PolarRegion) && center.sqrMagnitude > AdjacentBC.center.sqrMagnitude)) {

            fadded++;
            verts.Add(VertexDatabase[wBC3]);
            cols.Add(ColorDatabase[wBC3]);
            verts.Add(VertexDatabase[wBC2]);
            cols.Add(ColorDatabase[wBC2]);
            verts.Add(VertexDatabase[wBC1]);
            cols.Add(ColorDatabase[wBC1]);

            verts.Add(VertexDatabase[wBC4]);
            cols.Add(ColorDatabase[wBC4]);
            verts.Add(VertexDatabase[wBC3]);
            cols.Add(ColorDatabase[wBC3]);    
            verts.Add(VertexDatabase[wBC1]);
            cols.Add(ColorDatabase[wBC1]);
        }

        if ((AdjacentAB == null || AdjacentAB.FieldType != FieldType) ||
             (AdjacentAB != null && (FieldType == PLT_Biome.HillsAndMountains || FieldType == PLT_Biome.PolarRegion) && center.sqrMagnitude > AdjacentAB.center.sqrMagnitude)) {
            fadded++;
            verts.Add(VertexDatabase[wAB3]);
            cols.Add(ColorDatabase[wAB3]);
            verts.Add(VertexDatabase[wAB2]);
            cols.Add(ColorDatabase[wAB2]);
            verts.Add(VertexDatabase[wAB1]);
            cols.Add(ColorDatabase[wAB1]);

            verts.Add(VertexDatabase[wAB4]);
            cols.Add(ColorDatabase[wAB4]);
            verts.Add(VertexDatabase[wAB3]);
            cols.Add(ColorDatabase[wAB3]);
            verts.Add(VertexDatabase[wAB1]);
            cols.Add(ColorDatabase[wAB1]);


        }

        if ((AdjacentAC == null || AdjacentAC.FieldType != FieldType)||
             (AdjacentAC != null && (FieldType == PLT_Biome.HillsAndMountains || FieldType == PLT_Biome.PolarRegion) && center.sqrMagnitude > AdjacentAC.center.sqrMagnitude)) {
            fadded++;
            verts.Add(VertexDatabase[wCA3]);
            cols.Add(ColorDatabase[wCA3]);
            verts.Add(VertexDatabase[wCA2]);
            cols.Add(ColorDatabase[wCA2]);
            verts.Add(VertexDatabase[wCA1]);
            cols.Add(ColorDatabase[wCA1]);

            verts.Add(VertexDatabase[wCA4]);
            cols.Add(ColorDatabase[wCA4]);
            verts.Add(VertexDatabase[wCA3]);
            cols.Add(ColorDatabase[wCA3]);
            verts.Add(VertexDatabase[wCA1]);
            cols.Add(ColorDatabase[wCA1]);
        }

   

        return new PLT_Tuple<List<Vector3>, List<Color>>(verts, cols);


    }

}

public class PLT_FieldDecorator{

    protected PLT_Field m_field;

    private PLT_FieldDecorator() { }
    public PLT_FieldDecorator(PLT_Field field) {
        m_field = field;
    }
}

public class PLT_PathfindingField : PLT_FieldDecorator {

    // g,h,f for a*
    public float g;
    public float h;
    public float f;

    // Higher Value makes this field more expensive to traverse 
    public float TraversalCost;

    public PLT_PathfindingField CurrentParentField;

    public PLT_PathfindingField[] Neighbours;

    public PLT_PathfindingField(PLT_Field field, float travelCost) : base(field) {
        TraversalCost = travelCost;
    }

    /// <summary>
    /// Finds this fields neighbours in a field database and populates its Neighbours array for
    /// interconnected pathfinding data
    /// </summary>
    /// <param name="fieldDatabase"></param>
    /// <returns></returns>
    public bool ConnectToNeighbours(PLT_PathfindingField[] fieldDatabase) {
        Neighbours = new PLT_PathfindingField[3];
        int found = 0;
        for (int i = 0; i < fieldDatabase.Length; i++) {
            if (fieldDatabase[i].IsField(m_field.AdjacentAB)) {
                Neighbours[found++] = fieldDatabase[i]; 
            }
            if (fieldDatabase[i].IsField(m_field.AdjacentAC)) {
                Neighbours[found++] = fieldDatabase[i];
            }
            if (fieldDatabase[i].IsField(m_field.AdjacentBC)) {
                Neighbours[found++] = fieldDatabase[i];
            }
        }
        if (found == 3)
            return true;
        else {
            Debug.LogWarning("A PLT_Pathfinding field has not found three successors.");
            return false;
        }
    }

    /// <summary>
    /// Checks if this pathfinding field is decorating a certain field
    /// </summary>
    /// <param name="f"></param>
    /// <returns></returns>
    public bool IsField(PLT_Field f) {
        if (f == m_field)
            return true;
        return false;
    }

    /// <summary>
    /// checks if this field is obscured/blocked for pathfinding
    /// </summary>
    /// <returns></returns>
    public bool IsBlocked() {
        return m_field.Prop != null;
    }

    /// <summary>
    /// Gets this Fields Centerposition
    /// </summary>
    /// <returns></returns>
    public Vector3 GetCenterPosition() {
        return m_field.center;
    }

    /// <summary>
    /// Returns the actual PLT_Field instance behind this object
    /// </summary>
    /// <returns></returns>
    public PLT_Field GetActualPLTField() {
        return m_field;
    }
    
}

public struct PLT_Tuple<T, T2> {
    public T key;
    public T2 val;
    public PLT_Tuple(T key, T2 value) {
        this.key = key;
        this.val = value;
    }
}

[Serializable]
public enum PLT_Biome {
    None,
    DeepWater,
    Water,
    Sand,
    Grass,
    HillsAndMountains, 
    PolarRegion
}

[Serializable]
public struct PLT_PlanetProp {
    public GameObject SourcePrefab;
    public float SpawnChancePerField;
}

public delegate bool ExpandCondition(PLT_Field lhs, PLT_Field rhs);

