﻿using UnityEngine;
using System.Collections;
using EasyEditor;
using System.Collections.Generic;

public class PLT_PlanetPropDB : MonoBehaviour {

    public GameObject[] ForestTrees;

    public List<PLT_PlanetProp> BeachProps = new List<PLT_PlanetProp>();

    public List<PLT_PlanetProp> PolarProps = new List<PLT_PlanetProp>();

    public List<PLT_PlanetProp> GrassLandProps = new List<PLT_PlanetProp>();

    public GameObject SnowParticles;

    public GameObject Castle;

    public GameObject CityHouse;

}
