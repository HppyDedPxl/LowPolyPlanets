﻿using UnityEngine;
using System.Collections;

public class PLT_Config {

    public static Color COLOR_SEA = new Color(40 / 255f, 88 / 255f, 213 / 255f);
    public static Color COLOR_DEEPSEA = Color.blue;
    public static Color COLOR_GRASS = new Color(50 / 255f, 205 / 255f, 50 / 255f);
    public static Color COLOR_HILLS = new Color(154 / 255f, 205 / 255f, 50 / 255f);
    public static Color COLOR_MUD = new Color(165/255f,85/255f,42/255f);
    public static Color COLOR_BEACH = new Color(239 / 255f, 221 / 255f, 111 / 255f);

    public static Color COLOR_MOUNTAINBASE = new Color(165 / 255f, 85 / 255f, 42 / 255f);
    public static Color COLOR_MOUNTAINLOW = new Color(120 / 255f, 120 / 255f, 120 / 255f);
    public static Color COLOR_MOUNTAINHIGH = new Color(186 / 255f, 186 / 255f, 186 / 255f);
    public static Color COLOR_SNOW = new Color(237 / 255f, 237 / 255f, 237 / 255f);

    public static float BIOME_GLOBAL_HEIGHT_DEEPSEA = 2.05f;
    public static float BIOME_GLOBAL_HEIGHT_SEA = 2.1f;
    public static float BIOME_GLOBAL_HEIGHT_SAND = 2.2f;
    public static float BIOME_GLOBAL_HEIGHT_GRASS = 2.22f;

    public static int BIOME_POLAR_MINLAND = 13;
    public static int BIOME_POLAR_MAXLAND = 17;
    public static int BIOME_POLAR_MINSPREAD = 19;
    public static int BIOME_POLAR_MAXSPREAD = 30;

    public static float BIOME_MOUNTAIN_HEIGHT_HILLS = .13f;
    public static float BIOME_MOUNTAIN_HEIGHT_BASE = .2f;
    public static float BIOME_MOUNTAIN_HEIGHT_LOW = .25f;
    public static float BIOME_MOUNTAIN_HEIGHT_HIGH = .35f;

    public static float BIOME_MOUNTAIN_PROPS_FORESTCOUNT = 7;


    public static float PATHFINDING_COST_DEEPSEA = 4f;
    public static float PATHFINDING_COST_SEA = 2f;
    public static float PATHFINDING_COST_GRASS = 0.2f;
    public static float PATHFINDING_COST_HILLS = .5f;
    public static float PATHFINDING_COST_MOUNTAINS = 1.5f;
    public static float PATHFINDING_COST_BEACH = 0.5f;





}
