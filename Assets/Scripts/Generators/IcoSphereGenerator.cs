﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EasyEditor;

public struct IcoData {
    //public Mesh mesh;
    public Vector3[] Vertices;
    public PLT_Face[] faceData;
}



public class IcoSphereGenerator{


    // as seen in http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html

    private struct TriIndices{
        public int v1, v2, v3;

        public TriIndices(int v1, int v2, int v3){
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }
    }

    private int curIndex;
    private Dictionary<long, int> middlePointIndexCache;

    private List<PLT_Face> FinalFaces;

    private List<Vector3> Vertices;
    private List<Color> Colors;
    List<int> Indices;
    private List<Vector3> Normals;

    public IcoSphereGenerator() {
        Vertices = new List<Vector3>();
        Normals = new List<Vector3>();
        Indices = new List<int>();
        Colors = new List<Color>();
        FinalFaces = new List<PLT_Face>();
        middlePointIndexCache = new Dictionary<long, int>();
        curIndex = 0;
    }

    private int AddVertex(Vector3 p) {
        if(Vertices == null) {
            Debug.LogError("No vertex List initialized");
        }
        float l = Mathf.Sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
        Vector3 actPos = new Vector3(p.x / l, p.y / l, p.z / l);
        Normals.Add(actPos);
        Vertices.Add(actPos);
        return curIndex++;
    }
    
    

    private int GetMiddlePointIndexFromIndices(int i1, int i2, float offsetFromMiddle = 0) {
        bool lhsIsSmaller = i1 < i2;
        long smallerInt = lhsIsSmaller ? i1 : i2;
        long biggerInt = lhsIsSmaller ? i2 : i1;
        long key = (smallerInt << 32) + biggerInt;

        int ret;

        if (middlePointIndexCache.TryGetValue(key, out ret))
            return ret;

        // not cached, calculate
        Vector3 v1 = Vertices[i1];
        Vector3 v2 = Vertices[i2];

        Vector3 v3 = new Vector3(
            (v1.x + v2.x) / 2f,
            (v1.y + v2.y) / 2f,
            (v1.z + v2.z) / 2f
        );

 
        int i = AddVertex(v3);

        middlePointIndexCache.Add(key, i);
        return i;
    }

    private int CreateMiddlePointIndexFromIndices(int i1, int i2, float offsetFromMiddle = 0) {
        bool lhsIsSmaller = i1 < i2;

        // not cached, calculate
        Vector3 v1 = Vertices[i1];
        Vector3 v2 = Vertices[i2];

        Vector3 v3 = new Vector3(
            (v1.x + v2.x) / 2f,
            (v1.y + v2.y) / 2f,
            (v1.z + v2.z) / 2f
        );


        int i = AddVertex(v3);

        return i;
    }

  

    public static IcoData CreateIcoMesh(int refinementLevel){
        IcoSphereGenerator instance = new IcoSphereGenerator();

        float t = (1f + Mathf.Sqrt(5f)) / 2f;

        // Base Ico
        instance.AddVertex(new Vector3(-1,  t, 0)); //0
        instance.AddVertex(new Vector3(-t, 0,  1)); //11
        instance.AddVertex(new Vector3(0,  1,  t)); //5

        instance.AddVertex(new Vector3(-1, t, 0)); //0
        instance.AddVertex(new Vector3(0, 1, t)); //5
        instance.AddVertex(new Vector3(1, t, 0)); //1

        instance.AddVertex(new Vector3(-1, t, 0)); //0
        instance.AddVertex(new Vector3(1, t, 0)); //1
        instance.AddVertex(new Vector3(0, 1, -t)); //7

        instance.AddVertex(new Vector3(-1, t, 0)); //0
        instance.AddVertex(new Vector3(0, 1, -t)); //7
        instance.AddVertex(new Vector3(-t, 0, -1)); //10


        instance.AddVertex(new Vector3(-1, t, 0)); //0
        instance.AddVertex(new Vector3(-t, 0, -1)); //10
        instance.AddVertex(new Vector3(-t, 0, 1)); //11

        instance.AddVertex(new Vector3(1, t, 0)); //1
        instance.AddVertex(new Vector3(0, 1, t)); //5
        instance.AddVertex(new Vector3(t, 0, 1)); //9

        instance.AddVertex(new Vector3(0, 1, t)); //5
        instance.AddVertex(new Vector3(-t, 0, 1)); //11
        instance.AddVertex(new Vector3(0, -1, t)); //4

        instance.AddVertex(new Vector3(-t, 0, 1)); //11
        instance.AddVertex(new Vector3(-t, 0, -1)); //10
        instance.AddVertex(new Vector3(-1, -t, 0)); //2

        instance.AddVertex(new Vector3(-t, 0, -1)); //10
        instance.AddVertex(new Vector3(0, 1, -t)); //7
        instance.AddVertex(new Vector3(0, -1, -t)); //6

        instance.AddVertex(new Vector3(0, 1, -t)); //7
        instance.AddVertex(new Vector3(1, t, 0)); //1
        instance.AddVertex(new Vector3(t, 0, -1)); //8

        instance.AddVertex(new Vector3(1, -t, 0)); //3
        instance.AddVertex(new Vector3(t, 0, 1)); //9
        instance.AddVertex(new Vector3(0, -1, t)); //4

        instance.AddVertex(new Vector3(1, -t, 0)); //3
        instance.AddVertex(new Vector3(0, -1, t)); //4
        instance.AddVertex(new Vector3(-1, -t, 0)); //2

        instance.AddVertex(new Vector3(1, -t, 0)); //3
        instance.AddVertex(new Vector3(-1, -t, 0)); //2
        instance.AddVertex(new Vector3(0, -1, -t)); //6

        instance.AddVertex(new Vector3(1, -t, 0)); //3
        instance.AddVertex(new Vector3(0, -1, -t)); //6
        instance.AddVertex(new Vector3(t, 0, -1)); //8

        instance.AddVertex(new Vector3(1, -t, 0)); //3
        instance.AddVertex(new Vector3(t, 0, -1)); //8
        instance.AddVertex(new Vector3(t, 0, 1)); //9

        instance.AddVertex(new Vector3(0, -1, t)); //4
        instance.AddVertex(new Vector3(t, 0, 1)); //9
        instance.AddVertex(new Vector3(0, 1, t)); //5

        instance.AddVertex(new Vector3(-1, -t, 0)); //2
        instance.AddVertex(new Vector3(0, -1, t)); //4
        instance.AddVertex(new Vector3(-t, 0, 1)); //11

        instance.AddVertex(new Vector3(0, -1, -t)); //6
        instance.AddVertex(new Vector3(-1, -t, 0)); //2
        instance.AddVertex(new Vector3(-t, 0, -1)); //10

        instance.AddVertex(new Vector3(t, 0, -1)); //8
        instance.AddVertex(new Vector3(0, -1, -t)); //6
        instance.AddVertex(new Vector3(0, 1, -t)); //7

        instance.AddVertex(new Vector3(t, 0, 1)); //9
        instance.AddVertex(new Vector3(t, 0, -1)); //8
        instance.AddVertex(new Vector3(1, t, 0)); //1


        #region vertex lookup

        //instance.AddVertex(new Vector3(-1, t, 0)); //0
        //instance.AddVertex(new Vector3( 1,  t, 0)); //1
        //instance.AddVertex(new Vector3(-1, -t, 0)); //2
        //instance.AddVertex(new Vector3( 1, -t, 0)); //3
        //                                            
        //instance.AddVertex(new Vector3(0, -1,  t)); //4
        //instance.AddVertex(new Vector3(0, 1, t)); //5
        //instance.AddVertex(new Vector3(0, -1, -t)); //6
        //instance.AddVertex(new Vector3(0,  1, -t)); //7
        //                                            
        //instance.AddVertex(new Vector3( t, 0, -1)); //8
        //instance.AddVertex(new Vector3( t, 0,  1)); //9
        //instance.AddVertex(new Vector3(-t, 0, -1)); //10
        //instance.AddVertex(new Vector3(-t, 0, 1)); //11

        #endregion

        #region faces lookup

        //faces.Add(new TriIndices(0, 11, 5));
        //faces.Add(new TriIndices(0, 5, 1));
        //faces.Add(new TriIndices(0, 1, 7));
        //faces.Add(new TriIndices(0, 7, 10));
        //faces.Add(new TriIndices(0, 10, 11));

        //faces.Add(new TriIndices(1, 5, 9));
        //faces.Add(new TriIndices(5, 11, 4));
        //faces.Add(new TriIndices(11, 10, 2));
        //faces.Add(new TriIndices(10, 7, 6));
        //faces.Add(new TriIndices(7, 1, 8));

        //faces.Add(new TriIndices(3, 9, 4));
        //faces.Add(new TriIndices(3, 4, 2));
        //faces.Add(new TriIndices(3, 2, 6));
        //faces.Add(new TriIndices(3, 6, 8));
        //faces.Add(new TriIndices(3, 8, 9));

        //faces.Add(new TriIndices(4, 9, 5));
        //faces.Add(new TriIndices(2, 4, 11));
        //faces.Add(new TriIndices(6, 2, 10));
        //faces.Add(new TriIndices(8, 6, 7));
        //faces.Add(new TriIndices(9, 8, 1));

        #endregion


        List<TriIndices> faces = new List<TriIndices>();

        int z = 0;
        for (int i = 0; i < 20; i++) {
            faces.Add(new TriIndices(z, z + 1, z + 2));
            z += 3;
        }

        // Refine
        for (int i = 0; i < refinementLevel; i++) {
            List<TriIndices> refinedFaces = new List<TriIndices>();
            for (int j = 0; j < faces.Count; j++) {

                refinedFaces.Add(new TriIndices(faces[j].v1, instance.CreateMiddlePointIndexFromIndices(faces[j].v1, faces[j].v2), instance.CreateMiddlePointIndexFromIndices(faces[j].v3, faces[j].v1)));
                refinedFaces.Add(new TriIndices(faces[j].v2, instance.CreateMiddlePointIndexFromIndices(faces[j].v2, faces[j].v3), instance.CreateMiddlePointIndexFromIndices(faces[j].v1, faces[j].v2)));
                refinedFaces.Add(new TriIndices(faces[j].v3, instance.CreateMiddlePointIndexFromIndices(faces[j].v3, faces[j].v1), instance.CreateMiddlePointIndexFromIndices(faces[j].v2, faces[j].v3)));

                refinedFaces.Add(new TriIndices(
                    instance.CreateMiddlePointIndexFromIndices(faces[j].v1, faces[j].v2), 
                    instance.CreateMiddlePointIndexFromIndices(faces[j].v2, faces[j].v3), 
                    instance.CreateMiddlePointIndexFromIndices(faces[j].v3, faces[j].v1)
                ));


            }
            faces = refinedFaces;
        }


        // iterate over all faces and add borders and fix normals
        int originVertCount = faces.Count;
        for (int i = 0; i < originVertCount; i++) {

            // get indices of main face
            int a = faces[i].v1;
            int b = faces[i].v2;
            int c = faces[i].v3;

            // calculate center
            Vector3 faceCenter = instance.Middle(a, b, c);

            // Wall AB
            int wAB1 = instance.AddVertex(instance.Vertices[a]);
            int wAB2 = instance.AddVertex(instance.Vertices[a]);
            int wAB3 = instance.AddVertex(instance.Vertices[b]);
            int wAB4 = instance.AddVertex(instance.Vertices[b]);

            faces.Add(new TriIndices(wAB3, wAB2, wAB1));
            faces.Add(new TriIndices(wAB4, wAB3, wAB1));

            // Wall BC
            int wBC1 = instance.AddVertex(instance.Vertices[b]);
            int wBC2 = instance.AddVertex(instance.Vertices[b]);
            int wBC3 = instance.AddVertex(instance.Vertices[c]);
            int wBC4 = instance.AddVertex(instance.Vertices[c]);

            faces.Add(new TriIndices(wBC3, wBC2, wBC1));
            faces.Add(new TriIndices(wBC4, wBC3, wBC1));


            // Wall CA
            int wCA1 = instance.AddVertex(instance.Vertices[c]);
            int wCA2 = instance.AddVertex(instance.Vertices[c]);
            int wCA3 = instance.AddVertex(instance.Vertices[a]);
            int wCA4 = instance.AddVertex(instance.Vertices[a]);

            faces.Add(new TriIndices(wCA3, wCA2, wCA1));
            faces.Add(new TriIndices(wCA4, wCA3, wCA1));


            // Create the final Face
            instance.FinalFaces.Add(new PLT_Face(a, b, c, wAB1, wAB2, wAB3, wAB4,
                                                          wBC1 ,wBC2, wBC3, wBC4,
                                                          wCA1, wCA2, wCA3, wCA4, faceCenter));



        }


        for (int i = 0; i < faces.Count; i++) {
            instance.Indices.Add(faces[i].v1);
            instance.Indices.Add(faces[i].v2);
            instance.Indices.Add(faces[i].v3);
        }

       // Mesh ret = new Mesh();
       // ret.vertices = instance.Vertices.ToArray();
       // ret.triangles = instance.Indices.ToArray();

      //  ret.RecalculateBounds();
      //  ret.RecalculateNormals();

        instance.ConnectFacesToNeighbours();

        IcoData data = new IcoData() { Vertices = instance.Vertices.ToArray(), faceData = instance.FinalFaces.ToArray() };
        return data;
    }

    private Vector3 Middle(int a, int b, int c) {

        Vector3 sum = Vertices[a] + Vertices[b] + Vertices[c];
        return sum / 3;
    }

    private void AddColor(Color color) {
        Colors.Add(color);
    }

    public void ConnectFacesToNeighbours() {
        for (int i = 0; i < FinalFaces.Count; i++) {
            PLT_Face f = FinalFaces[i];
            Debug.DrawRay(f.center, ((Vertices[f.v3] - f.center) * -1),Color.red,2f);
            // get potential AB neighbour (reverse C)
            Vector3 temp =  f.center + ((Vertices[f.v3] - f.center) * -1);
            f.NeighbourAB = GetClosestFaceTo(temp);

            // get potential AC neighbour (reverse B)
            temp = f.center + ((Vertices[f.v2] - f.center)*-1);
            f.NeighbourAC = GetClosestFaceTo(temp);

            // get potential BC neighbour (reverse A)
            temp = f.center +  ((Vertices[f.v1] - f.center) * -1f);
            f.NeighbourCB = GetClosestFaceTo(temp);
        }
    }

    public PLT_Face GetFaceFromCenterPosition(Vector3 pos) {
        for (int i = 0; i < FinalFaces.Count; i++) {
            if(Vector3.SqrMagnitude(FinalFaces[i].center-pos) < 0.05f) {
                return FinalFaces[i];
            }
        }
        return null;
    }

    public PLT_Face GetClosestFaceTo(Vector3 pos) {
        int closest = 0;
        float closestSqrDist = 9999;
        for (int i = 0; i < FinalFaces.Count; i++) {
            float dist = Vector3.SqrMagnitude(FinalFaces[i].center - pos);
            if (dist < closestSqrDist) {
                closestSqrDist = dist;
                closest = i;
            }
        }
        return FinalFaces[closest];

    }
}
