﻿using UnityEngine;
using System.Collections;

public class RandomTester : MonoBehaviour {
    public int min;
    public int max;
    PLT_NoiseGen RNG;
	// Use this for initialization
	void Start () {
        RNG = new PLT_NoiseGen();
        RNG.Octaves = 4;
        RNG.Persistance = 1 / 64;
        RNG.Seed = "TestSeed";
    }
	
	// Update is called once per frame
	void Update () {

	}
}
