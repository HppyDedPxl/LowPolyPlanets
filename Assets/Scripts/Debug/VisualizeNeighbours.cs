﻿using UnityEngine;
using System.Collections;
using EasyEditor;

public class VisualizeNeighbours : MonoBehaviour {
    [Inspector(rendererType = "InlineClassRenderer")]
    public PLT_Face parentFace;
    public VisualizeNeighbours AB;
    public VisualizeNeighbours AC;
    public VisualizeNeighbours BC;

    public MakePlanet planet;

    Renderer r;
    Material m;

    public void Highlight() {

       

        if (r == null || m == null) {
            r = GetComponent<Renderer>();
            m = r.material;
        }
        m.color = Color.red;
        r.material = m;
    }

    public void DeHighlight() {
       
        if (r == null || m == null) {
            r = GetComponent<Renderer>();
            m = r.material;
        }
        m.color = Color.white;
        r.material = m;
    }

    void OnMouseEnter() {
        Highlight();
        AB.Highlight();
        AC.Highlight();
        BC.Highlight();
    }
    void OnMouseExit() {
        DeHighlight();
        AB.DeHighlight();
        AC.DeHighlight();
        BC.DeHighlight();
    }


    void OnMouseDown() {
        planet.RaiseFaces(new PLT_Face[] { parentFace, AC.parentFace, AB.parentFace, BC.parentFace }, 0.1f);
    }
}
