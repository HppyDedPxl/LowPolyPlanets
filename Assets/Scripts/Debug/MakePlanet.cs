﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class MakePlanet : MonoBehaviour {

    MeshRenderer m_meshRenderer;
    [HideInInspector]
    public MeshFilter m_meshFilter;
    public string Seed;
    public Material PlanetMaterial;
    public Material WaterMaterial;
    public Material PlanetOutlineMaterial;
    public Material PlanetAtmosphereMaterial;

    public IcoData icoData;
    List<GameObject> spawnedGOS;


    void Start() {
        Profiler.maxNumberOfSamplesPerFrame = 8000000;
        GeneratePlanet(4);
        GetComponent<PLT_EditorTerraformer>().Reterraform();
        GetComponent<PLT_EditorTerraformer>().PlaceProps();
        GetComponent<PLT_EditorTerraformer>().PlaceFactions();
        GetComponent<PLT_Planet>().UpdatePathfindingGraphs();

    }

    public void GeneratePlanet(int refinement) {


        if (spawnedGOS != null) {
            for (int i = 0; i < spawnedGOS.Count; i++) {
                DestroyImmediate(spawnedGOS[i]);
            }
            spawnedGOS.Clear();
        } else
            spawnedGOS = new List<GameObject>();

        m_meshFilter = GetComponent<MeshFilter>();
        m_meshRenderer = GetComponent<MeshRenderer>();
        m_meshRenderer.material = PlanetMaterial;
        icoData = IcoSphereGenerator.CreateIcoMesh(refinement);

        PLT_Planet p = gameObject.AddComponent<PLT_Planet>();
          

        // convert faces to field data
        List<PLT_Field> fields = new List<PLT_Field>();
        for (int i = 0; i < icoData.faceData.Length; i++) {
            PLT_Field f = new PLT_Field(icoData.faceData[i], PLT_Biome.None,p);
            icoData.faceData[i].ReferencedField = f;
            fields.Add(f);
        }

        for (int i = 0; i < fields.Count; i++) {
            fields[i].ConnectFields();
        }

        p.Initialize(fields, icoData.Vertices,Seed);

        // add water
        GameObject water = GameObject.Instantiate(Resources.Load<GameObject>("Environments/BlueWater/WaterSphere"));
        water.transform.localScale = new Vector3(4.25f, 4.25f, 4.25f);
        water.GetComponent<MeshRenderer>().material = WaterMaterial;
        water.transform.parent = p.transform;
        Destroy(water.GetComponent<Collider>());

        // Add Atmosphere
        GameObject Atmosphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Atmosphere.transform.localScale = new Vector3(5f, 5f, 5f);

        Atmosphere.GetComponent<MeshRenderer>().material = PlanetAtmosphereMaterial;
        Atmosphere.transform.parent = p.transform;
        Atmosphere.GetComponent<MeshRenderer>().receiveShadows = false;
        Atmosphere.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        Destroy(Atmosphere.GetComponent<Collider>());

        Destroy(this);

    }



    public void RaiseFaces (PLT_Face[] faces, float amount) {
        for (int i = 0; i < faces.Length; i++) {
           // RaiseFace(faces[i], amount);
        }

        m_meshFilter.mesh.RecalculateBounds();
        m_meshFilter.mesh.RecalculateNormals();
    }

}
