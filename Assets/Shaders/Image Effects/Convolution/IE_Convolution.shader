﻿Shader "Hidden/IE_Convolution"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			// Main Texture
			sampler2D _MainTex;
			// texel size so we know the uv-size of one fragment
			float4 _MainTex_TexelSize;

			// Convolution Matrix
			int m00, m01, m02,
				m10, m11, m12,
				m20, m21, m22;
			// matrix multiplier
			float c; 
				


			fixed4 frag (v2f i) : SV_Target
			{
				// Get the Pixels inside the Kernel, MainTex_TexelSize.x = the uv step needed to to one fragment on the x axis.
				// If you don't have the TexelSize you can calculate it by 1/RenderingSize of the surface
				// In post processing that would be 1/ScreenWidth for the x texel size and 1/ScreenHeight for the y texel size
				fixed4 c00 = tex2D(_MainTex, i.uv + float2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y));
				fixed4 c01 = tex2D(_MainTex, i.uv + float2(0					, _MainTex_TexelSize.y));
				fixed4 c02 = tex2D(_MainTex, i.uv + float2( _MainTex_TexelSize.x, _MainTex_TexelSize.y));

				fixed4 c10 = tex2D(_MainTex, i.uv + float2(-_MainTex_TexelSize.x, 0));
				fixed4 c11 = tex2D(_MainTex, i.uv + float2(0					, 0));
				fixed4 c12 = tex2D(_MainTex, i.uv + float2( _MainTex_TexelSize.x, 0));

				fixed4 c20 = tex2D(_MainTex, i.uv + float2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y));
				fixed4 c21 = tex2D(_MainTex, i.uv + float2(0					, -_MainTex_TexelSize.y));
				fixed4 c22 = tex2D(_MainTex, i.uv + float2( _MainTex_TexelSize.x, -_MainTex_TexelSize.y));

				// compute the final color
				return (
					c00 * m00 + c01 * m01 + c02 * m02 +
					c10 * m10 + c11 * m11 + c12 * m12 +
					c20 * m20 + c21 * m21 + c22 * m22
					) * c;

				
			}
			ENDCG
		}
	}
}
