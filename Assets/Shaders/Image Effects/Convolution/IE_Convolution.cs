﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct ConvolutionMatrix3x3
{
    public int  m00, m01, m02,
                m10, m11, m12,
                m20, m21, m22;
    public float c;

    public static bool operator==(ConvolutionMatrix3x3 lhs, ConvolutionMatrix3x3 rhs)
    {
        return !(lhs.m00 != rhs.m00 || lhs.m01 != rhs.m01 || lhs.m02 != rhs.m02 ||
           lhs.m10 != rhs.m10 || lhs.m11 != rhs.m11 || lhs.m12 != rhs.m12 ||
           lhs.m20 != rhs.m20 || lhs.m21 != rhs.m21 || lhs.m22 != rhs.m22 ||
           lhs.c != rhs.c);
    }

    public static bool operator !=(ConvolutionMatrix3x3 lhs, ConvolutionMatrix3x3 rhs)
    {
        return !(lhs == rhs);
    }

    public static ConvolutionMatrix3x3 Identity = new ConvolutionMatrix3x3()
    {
        m00 = 0, m01 = 0, m02 = 0,
        m10 = 0, m11 = 1, m12 = 0,
        m20 = 0, m21 = 0, m22 = 0,
        c = 1
    };

    public static ConvolutionMatrix3x3 Sharpen = new ConvolutionMatrix3x3()
    {
        m00 = 0, m01 = -1, m02 = 0,
        m10 = -1, m11 =  5, m12 = -1,
        m20 = 0, m21 = -1, m22 = 0,
        c = 1
    };

    public static ConvolutionMatrix3x3 BoxBlur = new ConvolutionMatrix3x3()
    {
        m00 = 1, m01 = 1, m02 = 1,
        m10 = 1, m11 = 1, m12 = 1,
        m20 = 1, m21 = 1, m22 = 1,
        c = 1/9f
    };

    public static ConvolutionMatrix3x3 GuassianBlur = new ConvolutionMatrix3x3()
    {
        m00 = 1, m01 = 2, m02 = 1,
        m10 = 2, m11 = 4, m12 = 2,
        m20 = 1, m21 = 2, m22 = 1,
        c = 1/16f
    };

    public static ConvolutionMatrix3x3 EdgeDetection = new ConvolutionMatrix3x3()
    {
        m00 = 0, m01 = 1, m02 = 0,
        m10 = 1, m11 = -4, m12 = 1,
        m20 = 0, m21 = 1, m22 = 0,
        c = 1
    };

}
[ExecuteInEditMode]
public class IE_Convolution : MonoBehaviour {

    public enum Convolution { Identity, BoxBlur, GuassianBlur, Sharpen, EdgeDetection, Custom }

    [SerializeField]
    public Convolution SetConvolution;
    Convolution m_conv;

    private ConvolutionMatrix3x3 originalConvolution;
    public ConvolutionMatrix3x3 currentConvolution;
    private Shader m_shader;
    private Material m_material;

	void Awake()
    {
        m_shader = Shader.Find("Hidden/IE_Convolution");
        m_material = new Material(m_shader);
    }

    void Update()
    {
        if(originalConvolution != currentConvolution)
        {
            SetConvolution = Convolution.Custom;
            originalConvolution = currentConvolution;
        }
        if(SetConvolution != Convolution.Custom)
        {
            ConvolutionMatrix3x3 target;
            switch (SetConvolution) {
                case Convolution.Identity:
                    target = ConvolutionMatrix3x3.Identity;
                    break;
                case Convolution.BoxBlur:
                    target = ConvolutionMatrix3x3.BoxBlur;
                    break;
                case Convolution.GuassianBlur:
                    target = ConvolutionMatrix3x3.GuassianBlur;
                    break;
                case Convolution.EdgeDetection:
                    target = ConvolutionMatrix3x3.EdgeDetection;
                    break;
                case Convolution.Sharpen:
                    target = ConvolutionMatrix3x3.Sharpen;
                    break;
                default:
                    target = ConvolutionMatrix3x3.Identity;
                    break;
                    

            }
            if(currentConvolution != target)
            {
                currentConvolution = target;
                originalConvolution = target;
            }
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        m_material.SetFloat("c", currentConvolution.c);

        m_material.SetInt("m00", currentConvolution.m00);
        m_material.SetInt("m01", currentConvolution.m01);
        m_material.SetInt("m02", currentConvolution.m02);
                          
        m_material.SetInt("m10", currentConvolution.m10);
        m_material.SetInt("m11", currentConvolution.m11);
        m_material.SetInt("m12", currentConvolution.m12);
                          
        m_material.SetInt("m20", currentConvolution.m20);
        m_material.SetInt("m21", currentConvolution.m21);
        m_material.SetInt("m22", currentConvolution.m22);

        Graphics.Blit(source, dest, m_material);

    }
}
