﻿using UnityEngine;
using System.Collections;

public class IE_Selector : MonoBehaviour {

    private Material m_material;
    private Shader m_shader;
    private Vector3 m_selectorWorldPos;
    public float SelectorRange = 5f;
    public Texture2D FogTexture;

    void Awake()
    {
        m_shader = Shader.Find("Hidden/HoloSelector");
        m_material = new Material(m_shader);
    }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        CustomBlitFrustumCorners(source, dest, m_material);
    }

    void Update()
    {
        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rh;
        if(Physics.Raycast(r, out rh, Mathf.Infinity))
        {
            m_selectorWorldPos = rh.point;
        }
    }

    void CustomBlitFrustumCorners(RenderTexture source, RenderTexture dest, Material mat)
    {


        // Calculate the viewfrustum corners
        Transform camTransform = Camera.main.transform;
        Camera camera = Camera.main;

        mat.SetVector("_SelectorWPos", m_selectorWorldPos);
        mat.SetFloat("_SelectorSize", SelectorRange);
        mat.SetTexture("_NormalTex", FogTexture);
        mat.SetFloat("_xOffset", mat.GetFloat("_xOffset") + .1f * Time.deltaTime);
        
        // Calculate center of our farPlane
        Vector3 farPlaneCenter = camTransform.position + camTransform.forward * camera.farClipPlane;

        // Calculate the half height and width of the far plane
        Vector3 halfHeightVector = camTransform.up * (Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad) * camera.farClipPlane);
        Vector3 halfWidthVector = camTransform.right * (Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad) * camera.farClipPlane * camera.aspect);


        // Calculate the four corners of the view frustum
        Vector3 topLeft = farPlaneCenter + halfHeightVector - halfWidthVector;
        Vector3 topRight = farPlaneCenter + halfHeightVector + halfWidthVector;
        Vector3 bottomLeft = farPlaneCenter - halfHeightVector - halfWidthVector;
        Vector3 bottomRight = farPlaneCenter - halfHeightVector + halfWidthVector;


        // When applying an image effect we shade a Quad with our rendered screen output (rendertexture) on it.

        // Set new rendertexture as active and feed the source texture into the material
        RenderTexture.active = dest;
        mat.SetTexture("_MainTex", source);

        // Low-Level Graphics Library calls

        GL.PushMatrix(); // Calculate MVP Matrix and push it to the GL stack
        GL.LoadOrtho(); // Set up Ortho-Perspective Transform

        m_material.SetPass(0); // start the first rendering pass

        GL.Begin(GL.QUADS); // Begin rendering quads


        GL.MultiTexCoord2(0, 0.0f, 0.0f); // prepare input struct (Texcoord0) for this vertex
        GL.MultiTexCoord(1, bottomLeft); // prepare input struct (Texcoord1) for this vertex
        GL.Vertex3(0.0f, 0.0f, 0.0f); // Finalize and submit this vertex for rendering (bottom left)

        GL.MultiTexCoord2(0, 1.0f, 0.0f); // prepare input struct (Texcoord0) for this vertex
        GL.MultiTexCoord(1, bottomRight); // prepare input struct (Texcoord1) for this vertex
        GL.Vertex3(1.0f, 0.0f, 0.0f); // Finalize and submit this vertex for rendering  (bottom right)

        GL.MultiTexCoord2(0, 1.0f, 1.0f); // prepare input struct (Texcoord0) for this vertex
        GL.MultiTexCoord(1, topRight); // prepare input struct (Texcoord1) for this vertex
        GL.Vertex3(1.0f, 1.0f, 0.0f); // Finalize and submit this vertex for rendering  (top right)

        GL.MultiTexCoord2(0, 0.0f, 1.0f); // prepare input struct (Texcoord0) for this vertex
        GL.MultiTexCoord(1, topLeft); // prepare input struct (Texcoord1) for this vertex
        GL.Vertex3(0.0f, 1.0f, 0.0f); // Finalize and submit this vertex for rendering (top left)

        // Finalize drawing the Quad
        GL.End();
        // Pop the matrices off the stack, as they are not needed any further
        GL.PopMatrix();
    }
}
