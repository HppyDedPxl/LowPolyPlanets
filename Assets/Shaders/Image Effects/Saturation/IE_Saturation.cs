﻿using UnityEngine;
using System.Collections;

public class IE_Saturation : MonoBehaviour {


    private Shader m_shader;
    private Material m_material;
    [Range(0,10)]
    public float Saturation;
    public bool HSVConversionMethod;
    void Awake()
    {
        m_shader = Shader.Find("Hidden/Saturation");
        m_material = new Material(m_shader);
    }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        m_material.SetFloat("_Saturation", Saturation);
        m_material.SetInt("_useHsvConversion", HSVConversionMethod ? 1 : 0);
        Graphics.Blit(source, dest, m_material);
    }
}
