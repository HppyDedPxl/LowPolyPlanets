﻿Shader "Hidden/Saturation"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			// Helper function to convert rgb values to hsv space
			float3 rgb2hsv(float3 i) {
				float h, s, v;


				float Cmax = max(i.r, max(i.g, i.b));
				float Cmin = min(i.r, min(i.g, i.b));
				float d = Cmax - Cmin;

				v = Cmax;

				if (Cmax != 0)
					s = d / Cmax;
				else {
					s = 0;
					h = -1;
				}

				if (i.r == Cmax)
					h = (i.g - i.b) / d;
				else if (i.g == Cmax)
					h = 2 + (i.b - i.r) / d;
				else
					h = 4 + (i.r - i.g) / d;

				h *= 60;
				if (h < 0) {
					h += 360;
		}

		return float3(h,s,v);
	}

			// Helper function to convert hsv values to rgb space
			float3 hsv2rgb(float3 i) {

		float h = i.x;
		float s = i.y;
		float v = i.z;

		if (s == 0)
			return float3(v, v, v);

		float chroma = v * s;
		float h0 = h / 60.0;

		float3 rgb0;
		float interm = chroma*(1 - abs(h0 % 2 - 1));

		if (h == -1)
			rgb0 = float3(0, 0, 0);
		else if (0 <= h0 && h0 < 1)
			rgb0 = float3(chroma, interm, 0);
		else if (1 <= h0 && h0 < 2)
			rgb0 = float3(interm, chroma, 0);
		else if (2 <= h0 && h0 < 3)
			rgb0 = float3(0, chroma, interm);
		else if (3 <= h0 && h0 < 4)
			rgb0 = float3(0, interm, chroma);
		else if (4 <= h0 && h0 < 5)
			rgb0 = float3(interm, 0, chroma);
		else if (5 <= h0 && h0 < 6)
			rgb0 = float3(chroma, 0, interm);

		float d = v - chroma;

		return float3(rgb0.r + d, rgb0.g + d, rgb0.b + d);

	}

			#include "UnityCG.cginc"
			int _useHsvConversion;

			// Changes the Saturation of an RGB color
			fixed3 ChangeSaturationBy(fixed3 rgb, float sat) {

				// determine the dominant color
				fixed cmax = max(rgb.r, max(rgb.g, rgb.b));

				// lower the non dominant colors, maintaining relative distance to each other
				return fixed3(
					saturate(cmax + (rgb.r - cmax) * sat),
					saturate(cmax + (rgb.g - cmax) * sat),
					saturate(cmax + (rgb.b - cmax) * sat)
				);
			}


			fixed _Saturation;
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				
				if(!_useHsvConversion)
					return float4(ChangeSaturationBy(col.rgb,_Saturation),1);
				else {
					fixed3 hsv = rgb2hsv(col.rgb);
					hsv.g *= _Saturation;
					return float4(hsv2rgb(hsv), 1);
				}
			}

			
			ENDCG
		}
	}
}
