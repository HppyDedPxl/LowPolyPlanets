﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/MF_RevealObject" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_ScreenStencilMask("Stencil Mask (DoNotSet)",2D) = "white" {}
		_AlphaTest("AlphaCutout",float) = 0.5
		_DitherNoise("DitherNoiseMask",2D) = "white" {}
	}
		SubShader{

		Pass{
		Name "ShadowCaster"
		Tags{ "LightMode" = "ShadowCaster" }

		Fog{ Mode Off }
		ZWrite On ZTest LEqual Cull Off
		Offset 1, 1

		CGPROGRAM

#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#pragma multi_compile_shadowcaster
#include "UnityCG.cginc"

	sampler2D _DitherNoise;
	sampler2D _ScreenStencilMask;
	float4 _DitherNoise_ST;
	float4 _ScreenStencilMask_TexelSize;
	fixed _AlphaTest;

	struct v2f {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
		float4 projScreenPos : TEXCOORD1;
	};

	v2f vert(appdata_base v) {
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv = TRANSFORM_TEX(v.texcoord, _DitherNoise);
		o.projScreenPos = ComputeScreenPos(o.pos);
		return o;
	}

	fixed4 frag(v2f i) : COLOR{

		float4 uvscreen = UNITY_PROJ_COORD(i.projScreenPos);

		float stencilValue = tex2Dproj(_ScreenStencilMask, uvscreen).r;
		float noiseValue = tex2D(_DitherNoise, i.uv + _Time / 8);
		float shadow = saturate(lerp(noiseValue*stencilValue,1.5,stencilValue));

		clip(stencilValue-_AlphaTest);
		
		SHADOW_CASTER_FRAGMENT(i)
	}
		ENDCG
	}

		Tags { "Queue" = "AlphaTest+3400" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 200
		CGPROGRAM
		// Physically based Standard lighting model
		#pragma surface surf Standard vertex:vert alphatest:_AlphaTest

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _DitherNoise;
		sampler2D _ScreenStencilMask;
		float4 _ScreenStencilMask_TexelSize;

		struct Input {
			float2 uv_MainTex;
			float2 uv_DitherNoise;
			float4 projScreenPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void vert(inout appdata_base v,out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.projScreenPos = ComputeScreenPos(mul(UNITY_MATRIX_MVP, v.vertex));
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			float4 uvscreen = UNITY_PROJ_COORD(IN.projScreenPos);

			float stencilValue = tex2Dproj(_ScreenStencilMask, uvscreen).r;
			float noiseValue = tex2D(_DitherNoise, IN.uv_DitherNoise + _Time/5);
			o.Albedo = c;
			o.Alpha = saturate(lerp(noiseValue*stencilValue,1.5,stencilValue));
			
		}
		ENDCG

		

	}
	FallBack "Diffuse"
}
