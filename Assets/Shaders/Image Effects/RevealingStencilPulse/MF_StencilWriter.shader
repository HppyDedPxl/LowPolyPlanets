﻿Shader "Hidden/MF_StencilWriter"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 ray : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float2 uv_depth : TEXCOORD1;
				float4 ray : TEXCOORD2;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.uv_depth = v.uv;
				o.ray = v.ray;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			float4 _SelectorWPos;
			float4 _MainTex_TexelSize;
			float _RevealerSize;
			float _PulseDistance;

			fixed4 frag(v2f i) : SV_Target
			{
				if (_PulseDistance == 0)
					return 0;

				// prevent screen flipping
#if UNITY_UV_STARTS_AT_TOP
				if (_MainTex_TexelSize.y < 0)
				i.uv.y = 1 - i.uv.y;
#endif

				// calculate the raw depth
				float rawDepth = DecodeFloatRG(tex2D(_CameraDepthTexture, i.uv_depth));
				// linear depth between 0 and 1
				float linDepth = Linear01Depth(rawDepth);
				// get this fragments local position inside the cameras view frustum
				float4 lsPos = linDepth * i.ray;
				// add the cameras world position to get this fragments world position
				float3 wsPos = _WorldSpaceCameraPos + lsPos;

				// calculate the world distance between our fragment and the hit position of whatever
				// we want to hightlight
				float dist = distance(wsPos.xyz, _SelectorWPos.xyz);


				float distanceToPulse = abs(dist - _PulseDistance);

				float distanceIterator = saturate(distanceToPulse / _RevealerSize);

				
				//return linDepth;
				//return dist;
				return 1-distanceIterator;
			}
			ENDCG
		}
	}
}
