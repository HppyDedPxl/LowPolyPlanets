﻿using UnityEngine;
using System.Collections;

public class MF_StencilRevealingObject : MonoBehaviour {

    public MeshRenderer affectedRenderer;
    public MF_StencilWriter StencilWriter;
    private Material m_materialInstance;

    void Awake()
    {
        if (!enabled) return;
        if(affectedRenderer == null)
        {
            affectedRenderer = GetComponent<MeshRenderer>();
        }
        if(StencilWriter == null)
        {
            StencilWriter = Camera.main.GetComponent<MF_StencilWriter>();
        }

        m_materialInstance = affectedRenderer.material;
        if(m_materialInstance.shader.name != "Custom/MF_RevealObject")
        {
            Shader s = Shader.Find("Custom/MF_RevealObject");
            m_materialInstance.shader = s;
        }
        
        m_materialInstance.SetTexture("_ScreenStencilMask", StencilWriter.RevealMask);
        affectedRenderer.material = m_materialInstance;
    }

    void Update()
    {
        m_materialInstance.SetTexture("_ScreenStencilMask", StencilWriter.RevealMask);
        affectedRenderer.material = m_materialInstance;
    }

}
