﻿Shader "Hidden/MF_Fog"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			// Helper function to convert rgb values to hsv space
		float3 rgb2hsv(float3 i) {
		float h, s, v;

		// Step 1
		float Cmax = max(i.r, max(i.g, i.b));
		float Cmin = min(i.r, min(i.g, i.b));
		float d = Cmax - Cmin;

		// Step 2
		v = Cmax;

		// Step 3
		if (Cmax != 0)
			s = d / Cmax;
		else {
			s = 0;
			// h = -1 is part of step 4, but we can conveniently set it here and return
			h = -1;
			return float3(h, s, v);
		}

		// Step 4
		if (i.r == Cmax)
			h = (i.g - i.b) / d;
		else if (i.g == Cmax)
			h = 2 + (i.b - i.r) / d;
		else
			h = 4 + (i.r - i.g) / d;

		h *= 60;

		// Hue has to be positive 
		if (h < 0) {
			h += 360;
		}

		return float3(h,s,v);
	}

	// Helper function to convert hsv values to rgb space
	float3 hsv2rgb(float3 i) {

		float h = i.x;
		float s = i.y;
		float v = i.z;

		if (s == 0)
			return float3(v, v, v);

		float chroma = v * s;
		float h0 = h / 60.0;

		float3 rgb0;
		float sndCol = chroma*(1 - abs(h0 % 2 - 1));

		if (h == -1)
			rgb0 = float3(0, 0, 0);
		else if (0 <= h0 && h0 < 1)
			rgb0 = float3(chroma, sndCol, 0);
		else if (1 <= h0 && h0 < 2)
			rgb0 = float3(sndCol, chroma, 0);
		else if (2 <= h0 && h0 < 3)
			rgb0 = float3(0, chroma, sndCol);
		else if (3 <= h0 && h0 < 4)
			rgb0 = float3(0, sndCol, chroma);
		else if (4 <= h0 && h0 < 5)
			rgb0 = float3(sndCol, 0, chroma);
		else if (5 <= h0 && h0 < 6)
			rgb0 = float3(chroma, 0, sndCol);

		float d = v - chroma;

		return float3(rgb0.r + d, rgb0.g + d, rgb0.b + d);

	}

	sampler2D _MainTex;
	float4 _MainTex_TexelSize;
	sampler2D _FogTexture;

	// Returns a box blurred version of the current fragment
	// using a 5x5 convolution matrix
	float4 GetBoxBlurredFragment(float2 uv) {
		float4 c10 = tex2D(_MainTex, uv + float2(-2 * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));
		float4 c11 = tex2D(_MainTex, uv + float2(-1 * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));
		float4 c12 = tex2D(_MainTex, uv + float2(0,							2 * _MainTex_TexelSize.y));
		float4 c13 = tex2D(_MainTex, uv + float2(1 * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));
		float4 c14 = tex2D(_MainTex, uv + float2(2 * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));

		float4 c20 = tex2D(_MainTex, uv + float2(-2 * _MainTex_TexelSize.x, 1 * _MainTex_TexelSize.y));
		float4 c21 = tex2D(_MainTex, uv + float2(-1 * _MainTex_TexelSize.x, 1 * _MainTex_TexelSize.y));
		float4 c22 = tex2D(_MainTex, uv + float2(0,						  1 * _MainTex_TexelSize.y));
		float4 c23 = tex2D(_MainTex, uv + float2(1 * _MainTex_TexelSize.x,  1 * _MainTex_TexelSize.y));
		float4 c24 = tex2D(_MainTex, uv + float2(2 * _MainTex_TexelSize.x,  1 * _MainTex_TexelSize.y));

		float4 c30 = tex2D(_MainTex, uv + float2(-2 * _MainTex_TexelSize.x, 0 * _MainTex_TexelSize.y));
		float4 c31 = tex2D(_MainTex, uv + float2(-1 * _MainTex_TexelSize.x, 0 * _MainTex_TexelSize.y));
		float4 c32 = tex2D(_MainTex, uv + float2(0,						  0));
		float4 c33 = tex2D(_MainTex, uv + float2(1 * _MainTex_TexelSize.x,  0 * _MainTex_TexelSize.y));
		float4 c34 = tex2D(_MainTex, uv + float2(2 * _MainTex_TexelSize.x,  0 * _MainTex_TexelSize.y));

		float4 c40 = tex2D(_MainTex, uv + float2(-2 * _MainTex_TexelSize.x, -1 * _MainTex_TexelSize.y));
		float4 c41 = tex2D(_MainTex, uv + float2(-1 * _MainTex_TexelSize.x, -1 * _MainTex_TexelSize.y));
		float4 c42 = tex2D(_MainTex, uv + float2(0,						  -1 * _MainTex_TexelSize.y));
		float4 c43 = tex2D(_MainTex, uv + float2(1 * _MainTex_TexelSize.x,  -1 * _MainTex_TexelSize.y));
		float4 c44 = tex2D(_MainTex, uv + float2(2 * _MainTex_TexelSize.x,  -1 * _MainTex_TexelSize.y));

		float4 c50 = tex2D(_MainTex, uv + float2(-2 * _MainTex_TexelSize.x, -2 * _MainTex_TexelSize.y));
		float4 c51 = tex2D(_MainTex, uv + float2(-1 * _MainTex_TexelSize.x, -2 * _MainTex_TexelSize.y));
		float4 c52 = tex2D(_MainTex, uv + float2(0,						  -2 * _MainTex_TexelSize.y));
		float4 c53 = tex2D(_MainTex, uv + float2(1 * _MainTex_TexelSize.x,  -2 * _MainTex_TexelSize.y));
		float4 c54 = tex2D(_MainTex, uv + float2(2 * _MainTex_TexelSize.x,  -2 * _MainTex_TexelSize.y));


		float4 blurredCol = (c10 + c11 + c12 + c13 + c14 +
			c20 + c21 + c22 + c23 + c24 +
			c30 + c31 + c32 + c33 + c34 +
			c40 + c41 + c42 + c43 + c44 +
			c50 + c51 + c52 + c53 + c54
			) / 25;

		return blurredCol;
	}

	// Returns a Sharpened version of the current fragment
	// using a 3x3 convolution matrix;
	float4 GetSharpenedFragment(float2 uv) {

		float4 c12 = tex2D(_MainTex, uv + float2(0, 1 * _MainTex_TexelSize.y));
		float4 c22 = tex2D(_MainTex, uv + float2(0, 0));
		float4 c32 = tex2D(_MainTex, uv + float2(0, -1 * _MainTex_TexelSize.y));

		float4 c21 = tex2D(_MainTex, uv + float2(-1 * _MainTex_TexelSize.x, 0));
		float4 c23 = tex2D(_MainTex, uv + float2(1 * _MainTex_TexelSize.x, 0));

		return (

			-c12 + 5 * c22 - c32
			- c21 - c23

			);


	}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
		

			fixed4 frag (v2f i) : SV_Target
			{
				// Calculate a sharpened, oversaturated fragment
				float4 colFull = GetSharpenedFragment(i.uv);

				// convert it to hsv and ramp up the saturation
				float3 orighsv = rgb2hsv(colFull);
				orighsv.g *= 1.3f;
				colFull = float4(hsv2rgb(orighsv),1);


				// Calculate a blurred, undersaturated fragment
				float4 blurredCol = GetBoxBlurredFragment(i.uv);

				// convert it to hsv and turn down the saturation and value
				float3 hsv = rgb2hsv(blurredCol.xyz);
				hsv.g *= .1f;
				hsv.b *= .3f;
				blurredCol = float4(hsv2rgb(hsv),1);

				float4 depth = tex2D(_FogTexture, i.uv);

				return lerp(blurredCol,colFull,depth);
			}
			ENDCG
		}
	}
}
