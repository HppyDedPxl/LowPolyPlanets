﻿Shader "Hidden/DepthBlur"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv_depth :TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float2 uv_depth : TEXCOORD1;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				// pass a copy of our UV's because depending on our rendering mode
				// we might have to flip our original uv's
				o.uv_depth = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			float4 _MainTex_TexelSize;
			float _DepthFade;

	

			fixed4 frag (v2f i) : SV_Target
			{
				// Prevent Flipping Forward/Deferred UVs on D3D
				// https://docs.unity3d.com/Manual/SL-PlatformDifferences.html
#if UNITY_UV_STARTS_AT_TOP
				if (_MainTex_TexelSize.y < 0)
					i.uv.y = 1 - i.uv.y;

#endif

				fixed4 col = tex2D(_MainTex, i.uv);
				
				// get the raw Depth relative to the camera
				float rawDepth = DecodeFloatRG(tex2D(_CameraDepthTexture, i.uv_depth));
				// normalize this between 0 .. 1 where 0 is at the near plane and 1 at the far plane
				float linDepth = Linear01Depth(rawDepth);

				// calculate the blur strength based on depth
				float BlurStrength = linDepth * _ProjectionParams.z * _DepthFade * _DepthFade;

				// blur
				float4 c10 = tex2D(_MainTex, i.uv + float2(-2 * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));
				float4 c11 = tex2D(_MainTex, i.uv + float2(-1 * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));
				float4 c12 = tex2D(_MainTex, i.uv + float2(0,							2 * _MainTex_TexelSize.y));
				float4 c13 = tex2D(_MainTex, i.uv + float2(1  * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));
				float4 c14 = tex2D(_MainTex, i.uv + float2(2  * _MainTex_TexelSize.x,	2 * _MainTex_TexelSize.y));

				float4 c20 = tex2D(_MainTex, i.uv + float2(-2 * _MainTex_TexelSize.x, 1 * _MainTex_TexelSize.y));
				float4 c21 = tex2D(_MainTex, i.uv + float2(-1 * _MainTex_TexelSize.x, 1 * _MainTex_TexelSize.y));
				float4 c22 = tex2D(_MainTex, i.uv + float2(0,						  1 * _MainTex_TexelSize.y));
				float4 c23 = tex2D(_MainTex, i.uv + float2(1 * _MainTex_TexelSize.x,  1 * _MainTex_TexelSize.y));
				float4 c24 = tex2D(_MainTex, i.uv + float2(2 * _MainTex_TexelSize.x,  1 * _MainTex_TexelSize.y));

				float4 c30 = tex2D(_MainTex, i.uv + float2(-2 * _MainTex_TexelSize.x, 0 * _MainTex_TexelSize.y));
				float4 c31 = tex2D(_MainTex, i.uv + float2(-1 * _MainTex_TexelSize.x, 0 * _MainTex_TexelSize.y));
				float4 c32 = tex2D(_MainTex, i.uv + float2(0,						  0));
				float4 c33 = tex2D(_MainTex, i.uv + float2(1 * _MainTex_TexelSize.x,  0 * _MainTex_TexelSize.y));
				float4 c34 = tex2D(_MainTex, i.uv + float2(2 * _MainTex_TexelSize.x,  0 * _MainTex_TexelSize.y));

				float4 c40 = tex2D(_MainTex, i.uv + float2(-2 * _MainTex_TexelSize.x, -1 * _MainTex_TexelSize.y));
				float4 c41 = tex2D(_MainTex, i.uv + float2(-1 * _MainTex_TexelSize.x, -1 * _MainTex_TexelSize.y));
				float4 c42 = tex2D(_MainTex, i.uv + float2(0,						  -1 * _MainTex_TexelSize.y));
				float4 c43 = tex2D(_MainTex, i.uv + float2(1 * _MainTex_TexelSize.x,  -1 * _MainTex_TexelSize.y));
				float4 c44 = tex2D(_MainTex, i.uv + float2(2 * _MainTex_TexelSize.x,  -1 * _MainTex_TexelSize.y));

				float4 c50 = tex2D(_MainTex, i.uv + float2(-2 * _MainTex_TexelSize.x, -2 * _MainTex_TexelSize.y));
				float4 c51 = tex2D(_MainTex, i.uv + float2(-1 * _MainTex_TexelSize.x, -2 * _MainTex_TexelSize.y));
				float4 c52 = tex2D(_MainTex, i.uv + float2(0,						  -2 * _MainTex_TexelSize.y));
				float4 c53 = tex2D(_MainTex, i.uv + float2(1 * _MainTex_TexelSize.x,  -2 * _MainTex_TexelSize.y));
				float4 c54 = tex2D(_MainTex, i.uv + float2(2 * _MainTex_TexelSize.x,  -2 * _MainTex_TexelSize.y));


				float4 blurredCol = (c10 + c11 + c12 + c13 + c14 +
					c20 + c21 + c22 + c23 + c24 +
					c30 + c31 + c32 + c33 + c34 +
					c40 + c41 + c42 + c43 + c44 +
					c50 + c51 + c52 + c53 + c54
				) / 25;



				return lerp(col, blurredCol, saturate(BlurStrength));

			

			}
			ENDCG
		}
	}
}
