﻿Shader "Hidden/IE_TopoSweep"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float2 uv_depth : TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;

			float4x4 _InverseViewMatrix;
			float4x4 _InverseProjectionMatrix;
			float4 _MainTex_TexelSize;


			sampler2D _ScanTexture;
			float4 _ScanColor;
			float4 _PulseOrigin;
			float _ScanHeadPct;
			float _TrailWidth;
			float _PulseDistance;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}


			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
			

				if (_PulseDistance == 0)
					return col;

				// prevent screen flipping
	#if UNITY_UV_STARTS_AT_TOP
					if (_MainTex_TexelSize.y < 0)
						i.uv.y = 1 - i.uv.y;
	#endif

					// Calculate the linear eye depth from the depth buffer
					// This Depth is Linear Eye depth takes only 0 - 1 values, so make sure to convert depth 
					// values to that format, then the function is defined as
					// LinearEyeDepth( float z ) { return 1.0 / (_ZBufferParams.z * z + _ZBufferParams.w); } 
					// where	 _ZBufferParams.x = (1-farClipDistance/nearClipDistance)
					// and where _ZBufferParams.y = (farClipDistance/nearClipDistance)
					// and where _ZBufferParams.z = (x/far)
					// and where _ZBufferParams.w = (y/far)
					float linDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv));
					// Get a vector that holds the FOV multipliers for our uv's
					float2 projectionMultipliers = float2(unity_CameraProjection._11, unity_CameraProjection._22);
					// convert from screenSpace to viewSpace by applying a reverse projection procedure
					float3 vpos = float3(
						// convert UV's so they represent a coordinate system with its origin in the middle
						(i.uv * 2 - 1) 
						// translate uv's back from our screens aspect ratio to a quadratic space
						/ projectionMultipliers, -1) // -1 denotes a depth of -1, so in the next step we translate AWAY from the origin
						// slide the whole coordinates by the depth in a reverese projection
						* linDepth;

					// convert from viewSpace to worldSpace
					float4 wsPos = mul(_InverseViewMatrix, float4(vpos, 1));

					// calculate the world distance between our fragment and the hit position of whatever
					// we want to hightlight
					float dist = distance(wsPos, _PulseOrigin.xyz);

					float distanceToPulse = dist - _PulseDistance;

					float distanceIterator = saturate(distanceToPulse / _TrailWidth);

					if (distanceIterator < 1) {
						if (distanceIterator > 1 - _ScanHeadPct)
							return _ScanColor;
						else {

							col = lerp(col, tex2D(_ScanTexture, i.uv * 5)*_ScanColor, distanceIterator);
						}
					}




						return col;
					}
					ENDCG
				}
	}
}
