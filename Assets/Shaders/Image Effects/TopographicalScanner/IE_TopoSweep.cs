﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class IE_TopoSweep : MonoBehaviour
{

    private Material m_material;
    private Shader m_shader;

    public Texture2D ScanTexture;
    private Vector4 m_pulseOrigin;
    public float ScannerSize;
    private float _pulseDistance;
    public float ScanHeadPct;
    public Color ScanColor;
    public float PulseSpeed;

    void Awake()
    {
        m_shader = Shader.Find("Hidden/IE_TopoSweep");
        m_material = new Material(m_shader);
    }

    void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        // Set public values
        m_material.SetTexture("_MainTex", source);
        m_material.SetTexture("_ScanTexture", ScanTexture);
        m_material.SetVector("_PulseOrigin", m_pulseOrigin);
        m_material.SetFloat("_TrailWidth", ScannerSize);
        m_material.SetFloat("_PulseDistance", _pulseDistance);
        m_material.SetFloat("_ScanHeadPct", ScanHeadPct);
        m_material.SetColor("_ScanColor", ScanColor);

        // Calculate inverse view matrix inverse and pass it to the shader
        m_material.SetMatrix("_InverseViewMatrix", Camera.main.worldToCameraMatrix.inverse);

        // draw
        Graphics.Blit(source, dest, m_material);
    }

    void Update()
    {
        // If clicky and we don't currently animate a pulse
        if (Input.GetMouseButtonDown(0) && _pulseDistance == 0)
        {
            // cast from mouse to gameworld
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rh;
            // We hit something
            if (Physics.Raycast(r, out rh, Mathf.Infinity))
            {
                // update origin position and start the animation
                m_pulseOrigin = rh.point;
                StartCoroutine(PulseReveal());
            }
        }
    }

    /// <summary>
    /// Animation Coroutine for the image effect
    /// </summary>
    /// <returns></returns>
    IEnumerator PulseReveal()
    {
        // while the pulse hasn't traveled fast enough
        while (_pulseDistance < 35)
        {
            // Increase the current distance of the pulse. 
            //ProTip: Scale the speed of the pulse with the traveled distance for a more uniform awesomeness and less waiting time!
            _pulseDistance += PulseSpeed * Time.deltaTime ;
            // wait 'till next frame
            yield return null;
        }
        // reset distance
        _pulseDistance = 0;
        yield break;
    }

}
