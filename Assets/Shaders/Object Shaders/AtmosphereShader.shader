﻿Shader "Custom/AtmosphereShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_RimPower("Rim Power", Range(0.5,8.0)) = 3.0
		_AtmoStrength ("StrengthOfAtmosphere", Range(0,1)) = 0.3
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent+3000"}
		LOD 200
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Unlit alpha:premul
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		float _RimPower;
		float4 _Color;
		float _AtmoStrength;
		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
		};
		half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten) {

			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;

			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Albedo = _Color.rgb * pow(rim, _RimPower)* _AtmoStrength;
			o.Alpha = saturate(pow(rim, _RimPower)) * _AtmoStrength;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
