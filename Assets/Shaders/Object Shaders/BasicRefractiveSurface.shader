﻿Shader "Custom/BasicRefractiveSurface" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_RefractionStrength("Refraction Strength" , float) = 2
		_RefractionMap ("Refraction",2D) = "bump" {}
		_DetailMap("Detail Texture",2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Transparent+3000" "Queue" = "Transparent" }
		LOD 200
		
		// Add a grab pass
		GrabPass{ Name "BASE" }
		Pass{ Name "BASE" }

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard noshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		// Grab texture from grab pass will be filled automatically
		sampler2D _GrabTexture;
		float4 _GrabTexture_TexelSize;

		sampler2D _RefractionMap;
		sampler2D _DetailMap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_RefractionMap;
			float2 uv_DetailMap;
			// important: Do not start this with uv, otherwise initialize unity will
			// try to automatically fill this value!
			float4 grabUV;
		};

		half _Glossiness;
		half _Metallic;
		half _RefractionStrength;
		fixed4 _Color;

		half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten) {
			return 1;
		}

		void vert(inout appdata_base v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);

			// Flip GrabTexture UVs if necessary
#if UNITY_UV_STARTS_AT_TOP
			float flip = -1.0;
#else
			float flip = 1.0;
#endif
			float4 wPos = mul(UNITY_MATRIX_MVP, v.vertex);

			o.grabUV.xy = (float2(wPos.x, wPos.y*flip) + wPos.w) * 0.5;
			o.grabUV.zw = wPos.zw;

		}

		void surf (Input IN, inout SurfaceOutputStandard o) {

			half2 bump = UnpackNormal(tex2D(_RefractionMap, IN.uv_RefractionMap));
			// calculate the offset by scaling the uv's from the refraction maps texture
			// dimensions to the grab textures dimensions (screen size)
			float2 offset = bump * _GrabTexture_TexelSize.xy * _RefractionStrength;

			IN.grabUV.xy = offset * IN.grabUV.z + IN.grabUV.xy;

			// Get the refracted pixel
			fixed4 c = tex2Dproj (_GrabTexture, UNITY_PROJ_COORD(IN.grabUV)) * _Color;
			fixed4 detail = tex2D(_DetailMap, IN.uv_DetailMap);

			o.Albedo = c.rgb;// *detail.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
