﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/IntersectionShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent+3400" }
		LOD 200
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert fullforwardshadows vertex:vert alpha:auto 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D_float _CameraDepthTexture;

		struct Input {
			float2 uv_MainTex;
			float4 projPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void vert(inout appdata_base v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.projPos  = ComputeScreenPos(mul(UNITY_MATRIX_MVP,v.vertex));
		}
		
		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			// Metallic and smoothness come from slider variables

			// calculate the depth of the current depth buffer fragment
			float bufferDepth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(IN.projPos)).r);
			// projected position Z is the depth of the fragment that we would write now
			float ownDepth = IN.projPos.z;
			
			

			float diff = abs(ownDepth - bufferDepth) / .2;
			float alphaFade = 0;
			if (diff <= 1) {
				alphaFade = 1-diff;
			}

			o.Albedo = float3(1,0,0);
			o.Alpha = alphaFade;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
