﻿Shader "Custom/ToonWaterAlphaBlended" {
	Properties {
		// Base Properties
		_Color ("Color", Color) = (1,1,1,1)
		
		_Alpha ("Water Transparency", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_WaterSpeed("Base Water Speed",float) = 1
		// Main Water Noise
		_NoiseTex("Water Distortion Texture",2D) = "white" {}
		[Toggle]
		_DistortWater("Distort Water",float) = 0
		_DistortWaterSpeed("Water Distortion Speed",float) = 1
		// Shore settings
		_ShoreTex("ShoreTex", 2D) = "white" {}
		_ShoreSpeed("Shore wave speed", float) = 3
		_ShoreThreshold("Shore Threshold",float) = 0.2
		_ShoreStrength("Shore Strength", float) = 1
		[Toggle]
		_DistortShore("Distort Shore", float) = 1
		_ShoreDistortion("Shore Distortion Strength", float) = 3
		[Toggle]
		_WhitenShore("Whiten Shore",float) = 1
		[Toggle]
		_FalloffShore("Shore Opacity Falloff",float) = 1
		// Wave Vertex Displacement
		_WaveDisplaceTex("Wave Displacement Texture", 2D) = "white" {}
		_WaveDisplaceStrength("Wave Displace Strength", float) = 0.4
		_WaveDisplaceSpeed("Wave Frequency" , float) = 1
		
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" "Queue"="Geometry+3000" "ForceNoShadowCasting" = "True" }
		LOD 200
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Toon vertex:vert alpha:auto keepalpha


		half4 LightingToon(SurfaceOutput s, half3 lightDir, half atten) {
			half NdotL = dot(s.Normal, lightDir);
			if (NdotL >= .99) {
				NdotL = 1;
			}
			if (NdotL > 0.8f) {
				NdotL = .88;
			}
			else if (NdotL > 0.7f) {
				NdotL = 0.8f;
			}
			else if (NdotL > 0.6f) {
				NdotL = 0.7f;
			}
			else if (NdotL > 0.3f) {
				NdotL = 0.5f;
			}
			else if (NdotL > 0.1f) {
				NdotL = 0.2f;
			}
			else NdotL = 0;

			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten);
			c.a = s.Alpha;
			return c;
		}


		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		fixed4 _Color;
		fixed _Alpha;
		
		
		sampler2D _MainTex;
		sampler2D _NoiseTex;
		sampler2D _ShoreTex;
		sampler2D _WaveDisplaceTex;

		sampler2D_float _CameraDepthTexture;

		fixed _WaterSpeed;
		fixed _WaveDisplaceStrength;
		fixed _WaveDisplaceSpeed;
		fixed _DistortWaterSpeed;
		fixed _ShoreThreshold;
		fixed _ShoreStrength;
		fixed _ShoreSpeed;
		fixed _ShoreDistortion;

		half _DistortShore;
		half _DistortWater;
		half _WhitenShore;
		half _FalloffShore;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NoiseTex;
			float2 uv_WaveDisplaceTex;
			float2 uv_ShoreTex;
			float4 projPos;
		};

		void vert(inout appdata_base v, out Input o) {
			// Initialize the input struct
			UNITY_INITIALIZE_OUTPUT(Input, o);
			// Calculate the current vertex position
			v.vertex += float4(v.normal.xyz * _WaveDisplaceStrength *  
				// sample the Displacement texture and use the red channel to determine displacement strength (grayscale texture)
				tex2Dlod(_WaveDisplaceTex, float4(v.texcoord.xy * _Time * (_WaveDisplaceSpeed / 10),0,0)).r,0);
			// calculate the screen position of the current vertex
			o.projPos = ComputeScreenPos(mul(UNITY_MATRIX_MVP, v.vertex));
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Calculate the water noise strength			  Animate water with passed time
			fixed4 noise = tex2D(_NoiseTex, IN.uv_NoiseTex + (_Time.x * _DistortWaterSpeed));
			// Calculate the water base texture				
			fixed4 waterCol = tex2D(_MainTex,
				// Animate with time ( 16 seemed a good divisor here
				IN.uv_MainTex + _Time.x * (_WaterSpeed / 16)
				// Distort the water texture if toggle is enabled
				+ (noise.xy * _DistortWater)) * _Color;

			// Calculate the shore base texture							
			fixed4 shoreCol = saturate(tex2D(_ShoreTex, IN.uv_ShoreTex +
				// Noiselate
				+(noise.xy * _ShoreDistortion) * _DistortShore
				+ _Time * _ShoreSpeed)
				// Make the shore more prominent
				* _ShoreStrength);

			// Base albedo of the base water texture
			o.Albedo = waterCol.rgb;

			// Get the Current Depth of the depth buffer
			float curDepth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(IN.projPos)).r);
			// Get the depth of our own fragment
			float ourDepth = IN.projPos.z;
			// Get the distance between the two fragments
			float dist = abs(curDepth - ourDepth) * _ShoreThreshold;

			// If the two are reasonably close, assume this is a shoreline
			if (dist < 1) {
				// Calculate a desired shore color for this fragment
				fixed3 shore = saturate((o.Albedo + shoreCol)
					// If desired add a white gradient to the shore
					+ (saturate((1 - dist)) * _WhitenShore));

				// Interpolate between normal and shore color using a cubic falloff (if wanted)
				if (_FalloffShore)
					o.Albedo = lerp(o.Albedo, shore, 1 - (dist*dist*dist*dist));
				else
					o.Albedo = shore;

			}
			
			
			o.Alpha = _Alpha;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
