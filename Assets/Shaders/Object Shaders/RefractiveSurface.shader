﻿Shader "Unlit/RefractiveSurface"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_RefractionMap("Refraction Map", 2D) = "bump" {}
		_RefractionStrength("Refraction Strength",float) = 2
	}
	SubShader
	{
		Tags { "RenderType"="Transparent""Queue" = "Transparent+3000" }
		LOD 100

		
		GrabPass{ Name "GRAB" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvbump : TEXCOORD3;
				float4 uvgrab : TEXCOORD2;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			float _RefractionStrength;
			sampler2D _MainTex;
			sampler2D _RefractionMap;
			sampler2D _GrabTexture;
			float4 _RefractionMap_ST;
			float4 _MainTex_ST;
		
			float4 _GrabTexture_TexelSize;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
#if UNITY_UV_STARTS_AT_TOP
				float flip = -1.0;
#else
				float flip = 1.0;
#endif
				o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*flip) + o.vertex.w) * 0.5;
				o.uvgrab.zw = o.vertex.zw;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvbump = TRANSFORM_TEX(v.uv, _RefractionMap);
				UNITY_TRANSFER_FOG(o, o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				half2 bump = UnpackNormal(tex2D(_RefractionMap, i.uvbump)).rg;
				
				float2 offset = bump * _GrabTexture_TexelSize.xy * _RefractionStrength;
				i.uvgrab.xy = offset * i.uvgrab.z + i.uvgrab.xy;

				half4 col = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.uvgrab));
			//	half4 tint = tex2D(_MainTex, i.uvmain);
				//col = lerp(col, tint, _TintAmt);
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
				//half2 bump = UnpackNormal(tex2D(_RefractionMap,i.uvbump)).rg;
				//float2 offset = bump * _RefractionStrength *  _GrabTexture_TexelSize.xy;
				//i.uvgrab.xy = offset * i.uvgrab.z + i.uvgrab.xy;
				//// sample the texture
				//fixed4 col = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.uvgrab));


				//// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				//return col;
			}
			ENDCG
		}
	}
}
