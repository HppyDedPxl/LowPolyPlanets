﻿Shader "Custom/PlanetShader" {
	Properties {
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf PlanetLight fullforwardshadows




		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float4 col : COLOR;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		half4 LightingPlanetLight(SurfaceOutput s, half3 lightDir, half atten) {
			half NdotL = dot(s.Normal, lightDir);
			if (NdotL >= .99) {
				NdotL = 1;
			}
			if (NdotL > 0.8f) {
				NdotL = .88;
			}
			else if (NdotL > 0.7f) {
				NdotL = 0.8f;
			}
			else if (NdotL > 0.6f) {
				NdotL = 0.7f;
			}
			else if (NdotL > 0.3f) {
				NdotL = 0.5f;
			}
			else if (NdotL > 0.1f) {
				NdotL = 0.2f;
			}
			else NdotL = 0;

			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten);
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = IN.col.rgb;

			o.Alpha = c.a;
		}
		
		ENDCG


		
			
	}
	FallBack "Diffuse"
}
