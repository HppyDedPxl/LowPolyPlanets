﻿Shader "Custom/PlanetOutlineShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)

	}
	SubShader {

		
			Tags{ "RenderType" = "Transparent" }
			LOD 200
			Cull Front	

		Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

		#pragma surface surf Lambert noshadow vertex:vert

		struct Input {
			float4 col : COLOR;
		};

		float4 _Color;
		void vert(inout appdata_full v) {
			v.vertex.xyz += v.normal * 0.02;
		}

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color;
			o.Alpha = 1;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
