# PlanetsGeneration

Generating procedural low poly planets in unity 3D

![](https://sharedat.allions.net/?iKbgVq23.jpg)

Includes Polar Regions
Includes Town Generation
Includes A* Pathfinding on the Planets surface